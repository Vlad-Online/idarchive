<?php

use Faker\Generator as Faker;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

$factory->define(\App\Symbolicfile::class, function (Faker $faker) {
	//Storage::fake('symbolicfiles');

	//$image = UploadedFile::fake()->image('1.jpg');
	$tempFile = $faker->image('/tmp', 640, 480);
	$res      = fopen($tempFile, 'rw');
	$image    = new \Illuminate\Http\Testing\File(basename($tempFile), $res);

	$filename = Str::random(20);

	// Make sure the filename does not exist, if it does, just regenerate
	while (Storage::disk(config('voyager.storage.disk'))->exists($filename . '.' . $image->getClientOriginalExtension()))
	{
		$filename = Str::random(20);
	}

	$image->storeAs(
		'symbolicfiles',
		$filename . '.' . $image->getClientOriginalExtension(),
		config('voyager.storage.disk', 'public')
	);

	$fileParams = [[
		'download_link' => "symbolicfiles" . DIRECTORY_SEPARATOR . $filename . '.' . $image->getClientOriginalExtension(),
		'original_name' => basename($tempFile)
	]];

	return [
		'title'         => $faker->company,
		'sort'          => 0,
		'symboltype_id' => 0,
		'visualkind_id' => 0,
		'filetype_id'   => 0,
		'symbolic_id'   => 0,
		'file'          => json_encode($fileParams),
		'fileprev' => json_encode($fileParams)
	];
});
