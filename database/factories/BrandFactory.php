<?php

use Faker\Generator as Faker;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

$factory->define(\App\Brand::class, function (Faker $faker) {

	if (rand(0, 1) > 0)
	{
		$tempFile = $faker->image('/tmp', 1024, 768);
		$res      = fopen($tempFile, 'rw');
		$image    = new \Illuminate\Http\Testing\File(basename($tempFile), $res);

		$filename = Str::random(20);

		// Make sure the filename does not exist, if it does, just regenerate
		while (Storage::disk(config('voyager.storage.disk'))->exists($filename . '.' . $image->getClientOriginalExtension()))
		{
			$filename = Str::random(20);
		}

		$image->storeAs(
			'brands',
			$filename . '.' . $image->getClientOriginalExtension(),
			config('voyager.storage.disk', 'public')
		);

		$fileParams = [[
			'download_link' => "brands" . DIRECTORY_SEPARATOR . $filename . '.' . $image->getClientOriginalExtension(),
			'original_name' => basename($tempFile)
		]];

		return [
			'title'           => $faker->company,
			'brandtype_id'    => 0,
			'main_page_image' => json_encode($fileParams)
		];
	}
	else
	{
		return [
			'title'        => $faker->company,
			'brandtype_id' => 0,
		];
	}
});
