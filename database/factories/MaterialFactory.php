<?php

use Faker\Generator as Faker;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

$factory->define(\App\Material::class, function (Faker $faker) {
	if (rand(0, 10) > 5)
	{
		return [
			'title' => $faker->sentence(2),
			'link'  => $faker->imageUrl(),
			'sort'  => 0
		];
	}
	else
	{
		$tempFile = $faker->image('/tmp', 640, 480);
		$res      = fopen($tempFile, 'rw');
		$image    = new \Illuminate\Http\Testing\File(basename($tempFile), $res);

		$filename = Str::random(20);

		// Make sure the filename does not exist, if it does, just regenerate
		while (Storage::disk(config('voyager.storage.disk'))->exists($filename . '.' . $image->getClientOriginalExtension()))
		{
			$filename = Str::random(20);
		}

		$image->storeAs(
			'materials',
			$filename . '.' . $image->getClientOriginalExtension(),
			config('voyager.storage.disk', 'public')
		);

		$fileParams = [[
			'download_link' => "materials" . DIRECTORY_SEPARATOR . $filename . '.' . $image->getClientOriginalExtension(),
			'original_name' => basename($tempFile)
		]];

		return [
			'title' => $faker->sentence(2),
			'file'  => json_encode($fileParams),
			'sort'  => 0
		];

	}


});
