<?php

use Faker\Generator as Faker;

$factory->define(\App\Brandnamealt::class, function (Faker $faker) {
    return [
        'title' => $faker->company
    ];
});
