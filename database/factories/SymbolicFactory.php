<?php

use Faker\Generator as Faker;

$factory->define(\App\Symbolic::class, function () {
	return [
		'status' => rand(0, 1),
		'sort'   => 0,
		'rating' => rand(1, 5),
	];
});
