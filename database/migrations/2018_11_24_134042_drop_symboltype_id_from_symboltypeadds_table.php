<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropSymboltypeIdFromSymboltypeaddsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('symboltypeadds', function (Blueprint $table) {
			$table->dropColumn('symboltype_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('symboltypeadds', function (Blueprint $table) {
			$table->integer('symboltype_id');
		});
	}
}
