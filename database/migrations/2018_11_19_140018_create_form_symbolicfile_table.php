<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFormSymbolicfileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('form_symbolicfile', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('form_id')->unsigned()->index();
			$table->integer('symbolicfile_id')->unsigned()->index();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('form_symbolicfile');
	}

}
