<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTimestampsFromPivotTables extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	private $tables = [
		'author_symbolic',
		'brand_industry',
		'brand_parent',
		'brand_predecessor',
		'brand_sector',
		'brand_targetaudience',
		'symbolic_year',
	];

	public function up()
	{
		foreach ($this->tables as $tableName)
		{
			Schema::table($tableName, function (Blueprint $table) {
				$table->dropTimestamps();
			});

		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		foreach ($this->tables as $tableName)
		{
			Schema::table($tableName, function (Blueprint $table) {
				$table->timestamps();
			});

		}
	}
}
