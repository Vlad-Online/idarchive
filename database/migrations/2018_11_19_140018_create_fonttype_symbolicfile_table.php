<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFonttypeSymbolicfileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('fonttype_symbolicfile', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('fonttype_id')->unsigned();
			$table->integer('symbolicfile_id')->unsigned();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('fonttype_symbolicfile');
	}

}
