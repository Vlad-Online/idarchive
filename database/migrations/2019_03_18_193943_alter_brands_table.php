<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('brands', function (Blueprint $table) {
        	$table->string('main_page_image', 64)->change();
        	$table->index('main_page_image');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::table('brands', function (Blueprint $table) {
		    $table->string('main_page_image', 128)->change();
		    $table->dropIndex('main_page_image');
	    });
    }
}
