<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSymbolicfileTechniqueTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('symbolicfile_technique', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('symbolicfile_id')->unsigned()->index();
			$table->integer('technique_id')->unsigned()->index();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('symbolicfile_technique');
	}

}
