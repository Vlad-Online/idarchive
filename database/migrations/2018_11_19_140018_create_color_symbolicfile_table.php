<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateColorSymbolicfileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('color_symbolicfile', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('color_id')->unsigned();
			$table->integer('symbolicfile_id')->unsigned();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('color_symbolicfile');
	}

}
