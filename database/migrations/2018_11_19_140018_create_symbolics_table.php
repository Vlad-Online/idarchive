<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSymbolicsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('symbolics', function(Blueprint $table)
		{
			$table->increments('id');
			$table->boolean('status')->default(1)->index();
			$table->integer('sort')->unsigned()->nullable()->index();
			$table->timestamps();
			$table->boolean('rating')->default(1)->index();
			$table->integer('year_id')->unsigned()->nullable();
			$table->integer('author_id')->unsigned()->nullable();
			$table->integer('brand_id')->unsigned()->index();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('symbolics');
	}

}
