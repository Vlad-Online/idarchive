<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDesignguidesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('designguides', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 50);
			$table->string('link', 512);
			$table->integer('filetype_id');
			$table->integer('sort')->nullable();
			$table->timestamps();
			$table->integer('symbolic_id')->unsigned()->index();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('designguides');
	}

}
