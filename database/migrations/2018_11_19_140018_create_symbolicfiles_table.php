<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSymbolicfilesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('symbolicfiles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
			$table->integer('language_id')->unsigned();
			$table->integer('symboltype_id')->unsigned();
			$table->integer('visualkind_id')->unsigned();
			$table->string('file', 512)->nullable();
			$table->string('fileprev', 512)->nullable();
			$table->integer('filetype_id')->unsigned()->nullable();
			$table->integer('sort')->unsigned()->nullable();
			$table->timestamps();
			$table->integer('symbolic_id')->unsigned()->index();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('symbolicfiles');
	}

}
