<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeNullableColumnDesignguidesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('designguides', function (Blueprint $table) {
			$table->string('link', 1024)->nullable()->change();
			$table->string('file', 1024)->nullable()->change();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('designguides', function (Blueprint $table) {
			$table->string('link', 512)->change();
			$table->string('file', 1024)->change();
		});
	}
}
