<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSymboltypeaddsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('symboltypeadds', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title', 50);
			$table->timestamps();
			$table->integer('symboltype_id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('symboltypeadds');
	}

}
