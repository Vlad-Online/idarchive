<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSymbolicfileSymboltypeaddTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('symbolicfile_symboltypeadd', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('symbolicfile_id')->unsigned()->nullable()->index();
			$table->integer('symboltypeadd_id')->unsigned()->nullable()->index();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('symbolicfile_symboltypeadd');
	}

}
