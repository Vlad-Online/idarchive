<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSimpleshapeSymbolicfileTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('simpleshape_symbolicfile', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('simpleshape_id')->unsigned()->index();
			$table->integer('symbolicfile_id')->unsigned()->index();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('simpleshape_symbolicfile');
	}

}
