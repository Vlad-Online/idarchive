<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materials', function (Blueprint $table) {
	        $table->increments('id');
	        $table->string('title', 100);
	        $table->string('link', 1024)->nullable();
	        $table->integer('filetype_id');
	        $table->integer('sort')->nullable();
	        $table->timestamps();
	        $table->integer('symbolic_id')->unsigned()->index();
	        $table->string('file', 1024)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materials');
    }
}
