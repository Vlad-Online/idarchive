<?php

use Illuminate\Database\Seeder;

class YearsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		for ($i = 1900; $i < 2019; $i++)
		{
			\App\Year::create(['title' => $i]);
		}
	}
}
