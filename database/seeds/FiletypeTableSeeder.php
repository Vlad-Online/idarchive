<?php

use Illuminate\Database\Seeder;

class FiletypeTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	private $data = [
		'pdf',
		'png',
		'jpg',
		'gif'
	];

	public function run()
	{
		foreach ($this->data as $title)
		{
			\App\Filetype::create(['title' => $title]);
		}
	}
}
