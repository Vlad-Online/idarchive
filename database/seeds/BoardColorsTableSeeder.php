<?php

use Illuminate\Database\Seeder;

class BoardColorsTableSeeder extends Seeder
{
	private $data = [
		'Purple' => 'c-bg-purple',
		'Pink'   => 'c-bg-pink',
		'Brown'  => 'c-bg-brown',
		'Blue'   => 'c-bg-blue',
		'Green'  => 'c-bg-green',
		'Yellow' => 'c-bg-yellow',
		'Orange' => 'c-bg-orange',
	];

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		foreach ($this->data as $title => $class)
		{
			\App\BoardColor::create([
				'title' => $title,
				'class' => $class
			]);
		}
	}
}
