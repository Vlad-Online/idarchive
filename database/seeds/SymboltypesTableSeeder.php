<?php

use Illuminate\Database\Seeder;

class SymboltypesTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	private $data = [
		'firm block (logo with sign)',
		'logotype',
		'sign',
		'emblem',
		'heraldry',
		'flag',
	];

	public function run()
	{
		foreach ($this->data as $title)
		{
			\App\Symboltype::create(['title' => $title]);
		}
	}
}
