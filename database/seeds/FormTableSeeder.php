<?php

use Illuminate\Database\Seeder;

class FormTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	private $data = [
		'no',
		'arrow',
		'circle',
		'hexagon',
		'outline',
		'oval',
		'ring',
	];

	public function run()
	{
		foreach ($this->data as $title)
		{
			\App\Form::create(['title' => $title]);
		}

	}
}
