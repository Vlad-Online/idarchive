<?php

use Illuminate\Database\Seeder;

class LanguageTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	private $data = [
		'Russian',
		'English'
	];

	public function run()
	{
		foreach ($this->data as $lang)
		{
			\App\Language::create(['title' => $lang]);
		}

	}
}
