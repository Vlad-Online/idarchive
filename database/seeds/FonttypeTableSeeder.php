<?php

use Illuminate\Database\Seeder;

class FonttypeTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	private $data = [
		'no(default)',
		'sans serif',
		'serif',
		'slab serif',
		'script',
		'blackletter',
		'mono',
		'hand',
		'decorative',
	];

	public function run()
	{
		foreach ($this->data as $title)
		{
			\App\Fonttype::create(['title' => $title]);
		}
	}
}
