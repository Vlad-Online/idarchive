<?php

use Illuminate\Database\Seeder;

class TechniquesTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	private $data = [
		'ombre',
		'double letter',
		'negative space',
		'paint',
		'parallel slits',
		'plastic bends',
		'paired figures',
		'tapes',
		'curls',
	];

	public function run()
	{
		foreach ($this->data as $title)
		{
			\App\Technique::create(['title' => $title]);
		}
	}
}
