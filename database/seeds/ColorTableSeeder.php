<?php

use Illuminate\Database\Seeder;

class ColorTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	private $data = [
		'black',
		'white',
		'red',
		'orange',
		'yellow',
		'lime',
		'green',
		'turquoise',
		'sky',
		'blue',
		'violet',
		'purple',
		'brown',
		'gold',
		'cream',
		'pink',
		'aqua',
		'grey',
		'colorfull',
		'texture',
		'b/w',
	];

	public function run()
	{
		$faker = \Faker\Factory::create();
		foreach ($this->data as $title)
		{
			\App\Color::create([
				'title' => $title,
				'code'  => $faker->hexColor
			]);
		}

	}
}
