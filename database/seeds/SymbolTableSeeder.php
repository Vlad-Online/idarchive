<?php

use Illuminate\Database\Seeder;

class SymbolTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	private $data = [
		'no',
		'su',
		'shield',
		'horse',
		'crown'
	];

	public function run()
	{
		foreach ($this->data as $title)
		{
			\App\Symbol::create(['title' => $title]);
		}
	}
}
