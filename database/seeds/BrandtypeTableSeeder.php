<?php

use Illuminate\Database\Seeder;

class BrandtypeTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	private $data = [
		'Corporation',
		'Company',
		'Government',
		'Consortium',
		'Non Profit Organization',
		'Trademark',
		'Content'
	];

	public function run()
	{
		foreach ($this->data as $title)
		{
			\App\Brandtype::create(['title' => $title]);
		}
	}
}
