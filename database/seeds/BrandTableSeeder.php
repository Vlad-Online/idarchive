<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class BrandTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		// Clear files
		Storage::disk(config('voyager.storage.disk'))->deleteDirectory('designguides');
		Storage::disk(config('voyager.storage.disk'))->deleteDirectory('symbolicfiles');

		$brandtypes      = \App\Brandtype::all();
		$countries       = \App\Country::with('region')->get();
		$sectors         = \App\Sector::all();
		$industries      = \App\Industry::all();
		$targetAudiences = \App\Targetaudience::all();
		$years           = \App\Year::all();
		$authors         = \App\Author::all();
		$filetypes       = \App\Filetype::all();
		$languages       = \App\Language::all();
		$symboltypes     = \App\Symboltype::all();
		$symboltypeadds  = \App\Symboltypeadd::all();
		$fonttypes       = \App\Fonttype::all();
		$colors          = \App\Color::all();
		$visualkinds     = \App\Visualkind::all();
		$technique       = \App\Technique::all();
		$forms           = \App\Form::all();
		$simpleshapes    = \App\Simpleshape::all();
		$symbols         = \App\Symbol::all();

		factory(\App\Brand::class, 30)->create()->each(function (\App\Brand $brand)
		use (
			$brandtypes, $countries, $sectors, $industries, $targetAudiences, $years, $authors, $filetypes,
			$languages, $symboltypes, $symboltypeadds, $fonttypes, $colors, $visualkinds, $technique, $forms,
			$simpleshapes, $symbols
		) {
			$brand->brandtype_id = $brandtypes->random()->id;

			$country           = $countries->random();
			$brand->country_id = $country->id;
			$brand->region_id  = $country->region->id;

			$brand->sectors()->saveMany($sectors->random(rand(1, 3)));

			$brand->industries()->saveMany($industries->random(rand(1, 3)));

			$brand->targetaudiences()->saveMany($targetAudiences->random(rand(1, 5)));

			$brand->site()->associate(factory(\App\Site::class)->create(['title' => $brand->title]));

			$brand->predecessor()->save(factory(\App\Brand::class)->make());

			$brand->parent()->save(factory(\App\Brand::class)->make());

			$brand->brandnamealts()->saveMany(factory(\App\Brandnamealt::class, 3)->make());

			$brand->symbolics()->saveMany(factory(App\Symbolic::class, 5)->create(
				['brand_id' => $brand->id]
			)
				->each(function (\App\Symbolic $symbolic) use (
					$brandtypes, $countries, $sectors, $industries, $targetAudiences, $years, $authors, $filetypes,
					$languages, $symboltypes, $symboltypeadds, $fonttypes, $colors, $visualkinds, $technique, $forms,
					$simpleshapes, $symbols
				) {
					$symbolic->years()->saveMany($years->random(rand(1, 3)));
					$symbolic->authors()->saveMany($authors->random(rand(1, 3)));
					$symbolic->designguides()->saveMany(factory(\App\Designguide::class, rand(1, 3))->create([
						'symbolic_id' => $symbolic->id,
						'filetype_id' => 0
					])
						->each(function (\App\Designguide $designguide) use (
							$brandtypes, $countries, $sectors, $industries, $targetAudiences, $years, $authors, $filetypes,
							$languages, $symboltypes, $symboltypeadds, $fonttypes, $colors, $visualkinds, $technique, $forms,
							$simpleshapes, $symbols
						) {
							$designguide->filetype()->associate($filetypes->random());
						}));

					$symbolic->materials()->saveMany(factory(\App\Material::class, rand(1, 3))->create([
						'symbolic_id' => $symbolic->id,
						'filetype_id' => 0
					])
						->each(function (\App\Material $material) use (
							$brandtypes, $countries, $sectors, $industries, $targetAudiences, $years, $authors, $filetypes,
							$languages, $symboltypes, $symboltypeadds, $fonttypes, $colors, $visualkinds, $technique, $forms,
							$simpleshapes, $symbols
						) {
							$material->filetype()->associate($filetypes->random());
						}));


					$symbolic->symbolicfiles()->saveMany(factory(\App\Symbolicfile::class, 2)->create()
						->each(function (\App\Symbolicfile $symbolicfile) use (
							$brandtypes, $countries, $sectors, $industries, $targetAudiences, $years, $authors, $filetypes,
							$languages, $symboltypes, $symboltypeadds, $fonttypes, $colors, $visualkinds, $technique, $forms,
							$simpleshapes, $symbols
						) {
							$symbolicfile->language()->attach($languages->random()->id);
							$symbolicfile->symboltype()->associate($symboltypes->random());
							$symbolicfile->symboltypeadds()->saveMany($symboltypeadds->random(rand(1, 3)));
							$symbolicfile->fonttypes()->saveMany($fonttypes->random(rand(1, 3)));
							$symbolicfile->colors()->saveMany($colors->random(rand(1, 3)));
							$symbolicfile->visualkind()->associate($visualkinds->random());
							$symbolicfile->techniques()->saveMany($technique->random(rand(1, 3)));
							$symbolicfile->forms()->saveMany($forms->random(rand(1, 3)));
							$symbolicfile->simpleshapes()->saveMany($simpleshapes->random(rand(1, 3)));
							$symbolicfile->symbols()->saveMany($symbols->random(rand(1, 3)));
							$symbolicfile->filetype()->associate($filetypes->random());
						}));
				}));

			$brand->save();
		});
	}
}
