<?php

use Illuminate\Database\Seeder;

class SymboltypeaddsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	private $data = [
		'no',
		'descriptor',
		'tagline',
		'slogan',
		'data',
		'flag',
	];

	public function run()
	{
		foreach ($this->data as $title)
		{
			\App\Symboltypeadd::create(['title' => $title]);
		}
	}
}
