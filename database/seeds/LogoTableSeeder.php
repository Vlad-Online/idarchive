<?php

use Illuminate\Database\Seeder;

class LogoTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	private $data = [
		'[{"download_link":"logos\/November2018\/usK1afXxkyVp2DoipfMb.svg","original_name":"cross.svg"}]',
		'[{"download_link":"logos\/November2018\/gSMRFeQvIRLA5XEwZRPI.svg","original_name":"jewish.svg"}]',
		'[{"download_link":"logos\/November2018\/DSEIjJBktr2zKuDNC4R9.svg","original_name":"massonery.svg"}]'

	];

	public function run()
	{
		foreach ($this->data as $path)
		{
			\App\Logo::create(['path' => $path]);
		}
	}
}
