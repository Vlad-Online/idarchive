<?php

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\Role;
use App\User;

class UsersTableSeeder extends Seeder
{
	/**
	 * Auto generated seed file.
	 *
	 * @return void
	 */
	public function run()
	{
		if (User::count() == 0)
		{
			$role = Role::where('name', 'admin')->firstOrFail();

			$user = User::create([
				'name'           => 'Admin',
				'email'          => 'vlad-online@mail.ru',
				'password'       => '$2y$10$FzFbS8.fbBLTaSzwe1JeEenIWJhBFM7Vea3o7gIELFFbr5QLwAKQW', //bcrypt('password'),
				'remember_token' => str_random(60),
				'role_id'        => $role->id,
			]);
			$user->subscription()->create(['value' => 1]);
		}
	}
}
