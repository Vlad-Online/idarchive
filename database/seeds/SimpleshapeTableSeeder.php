<?php

use Illuminate\Database\Seeder;

class SimpleshapeTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	private $data = [
		'no',
		'circle',
		'elipse',
		'square',
		'arrow	'
	];

	public function run()
	{
		foreach ($this->data as $title) {
			\App\Simpleshape::create(['title' => $title]);
		}
	}
}
