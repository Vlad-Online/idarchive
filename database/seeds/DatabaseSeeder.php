<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
	 * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
		$this->call(VoyagerDatabaseSeeder::class);
		$this->call(VoyagerDummyDatabaseSeeder::class);
		$this->call(UsersTableSeeder::class);
		$this->call(AuthorTableSeeder::class);
		$this->call(BrandtypeTableSeeder::class);
		$this->call(ColorTableSeeder::class);
		$this->call(LogoTableSeeder::class);
		$this->call(CountryRegionTableSeeder::class);
		$this->call(FiletypeTableSeeder::class);
		$this->call(FonttypeTableSeeder::class);
		$this->call(FormTableSeeder::class);
		$this->call(SectorIndustryTableSeeder::class);
		$this->call(LanguageTableSeeder::class);
		$this->call(SimpleshapeTableSeeder::class);
		$this->call(SymbolTableSeeder::class);
		$this->call(SymboltypeaddsTableSeeder::class);
		$this->call(SymboltypesTableSeeder::class);
		$this->call(TargetaudiencesTableSeeder::class);
		$this->call(TechniquesTableSeeder::class);
		$this->call(VisualkindsTableSeeder::class);
		$this->call(YearsTableSeeder::class);
		$this->call(BoardColorsTableSeeder::class);
		$this->call(BrandTableSeeder::class);
    }
}
