<?php

use Illuminate\Database\Seeder;

class VisualkindsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	private $data = [
		'static',
		'dynamic'
	];

	public function run()
	{
		foreach ($this->data as $title)
		{
			\App\Visualkind::create(['title' => $title]);
		}

	}
}
