<?php

use Illuminate\Database\Seeder;

class CountryRegionTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	private $data = [
		'Europe'                        => [
			'Albania',
			'Andorra',
			'Austria',
			'Belarus',
			'Bosnia',
			'Croatia',
			'Czech Republic',
			'Denmark',
			'Faroe Islands',
			'Finland',
			'France',
			'Germany',
			'Gibraltar',
			'Greece',
			'Guerney and Alderney',
			'Hungary',
			'Iceland',
			'Italy',
			'Jersey',
			'Kosovo',
			'Liechtenstein',
			'Macedonia',
			'Man, Island of',
			'Moldova',
			'Monaco',
			'Montenegro',
			'Norway',
			'Poland',
			'Portugal',
			'Russia',
			'San Marino',
			'Serbia',
			'Spain',
			'Svalbard and Jan Mayen Islands',
			'Switzerland',
			'Turkey',
			'Ukraine',
			'United Kingdom',
			'Vatican City State (Holy See)',
		],
		'Africa'                        => [
			'Algeria',
			'Angola',
			'Benin',
			'Botswana',
			'Burkina Faso',
			'Burundi',
			'Cameroon',
			'Cape Verde',
			'Central African Republic',
			'Chad',
			'Comoros',
			'Congo (Brazzaville)',
			'Congo, Democratic Republic of the',
			'Cote d\'Ivoire (Ivory Coast)',
			'Djibouti',
			'Egypt',
			'Equatorial Guinea',
			'Eritrea',
			'Ethiopia',
			'Gabon',
			'Gambia',
			'Ghana',
			'Guinea',
			'Guinea-Bissau',
			'Kenya',
			'Lesotho',
			'Liberia',
			'Libyan Arab Jamahiriya',
			'Madagascar',
			'Malawi',
			'Mali',
			'Mauritania',
			'Mauritius',
			'Mayotte',
			'Morroco',
			'Mozambique',
			'Namibia',
			'Niger',
			'Nigeria',
			'Reunion',
			'Rwanda',
			'Saint Helena',
			'Sao Tome and Principe',
			'Senegal',
			'Seychelles',
			'Sierra Leone',
			'Somalia',
			'South Africa',
			'South Sudan',
			'Sudan',
			'Swaziland',
			'Tanzania, United Republic of',
			'Togo',
			'Tunisia',
			'Uganda',
			'Western Sahara',
			'Zambia',
			'Zimbabwe',
		],
		'Middle East'                   => [
			'Bahrain',
			'Iraq',
			'Iran',
			'Israel',
			'Jordan',
			'Kuwait',
			'Lebanon',
			'Oman',
			'Palestine',
			'Qatar',
			'Saudi Arabia',
			'Syria',
			'United Arab Emirates',
			'Yemen',
		],
		'Asia'                          => [
			'Afganistan',
			'Armenia',
			'Azerbaijan',
			'Bangladesh',
			'Bhutan',
			'Brunei Darussalam',
			'Cambodia',
			'China',
			'Georgia',
			'Hong Kong',
			'India',
			'Indonesia',
			'Japan',
			'Kazakhstan',
			'Korea, North',
			'Korea, South',
			'Kyrgyzstan',
			'Laos',
			'Macao',
			'Malaysia',
			'Maldives',
			'Mongolia',
			'Myanmar (ex-Burma)',
			'Nepal',
			'Pakistan',
			'Philippines',
			'Singapore',
			'Sri Lanka (ex-Ceilan)',
			'Taiwan',
			'Tajikistan',
			'Thailand',
			'Timor Leste (West)',
			'Turkmenistan',
			'Uzbekistan',
			'Vietnam',
		],
		'Oceania'                       => [
			'Australia',
			'Fiji',
			'French Polynesia',
			'Guam',
			'Kiribati',
			'Marshall Islands',
			'Micronesia',
			'New Caledonia',
			'New Zealand',
			'Papua New Guinea',
			'Samoa',
			'Samoa, American',
			'Solomon, Islands',
			'Tonga',
			'Vanuatu',
		],
		'Northern America'              => [
			'Bermuda',
			'Canada',
			'Greenland',
			'Saint Pierre and Miquelon',
			'United States',
		],
		'Central America and Caribbean' => [
			'Anguilla',
			'Antigua and Barbuda',
			'Aruba',
			'Bahamas',
			'Barbados',
			'Belize',
			'Bonaire, Saint Eustatius and Saba',
			'British Virgin Islands',
			'Cayman Islands',
			'Costa Rica',
			'Cuba',
			'Curaçao',
			'Dominica',
			'Dominican Republic',
			'El Salvador',
			'Grenada',
			'Guadeloupe',
			'Guatemala',
			'Haiti',
			'Honduras',
			'Jamaica',
			'Martinique',
			'Mexico',
			'Monserrat',
			'Nicaragua',
			'Panama',
			'Puerto Rico',
			'Saint Lucia',
			'Saint Martin',
			'Saint Vincent and the Grenadines',
			'Saint-Barthélemy',
			'Sint Maarten',
			'St. Kitts and Nevis',
			'Trinidad and Tobago',
			'Turks and Caicos Islands',
			'Virgin Islands (US)',
		],
		'South America'                 => [
			'Argentina',
			'Bolivia',
			'Brazil',
			'Chile',
			'Colombia',
			'Ecuador',
			'Falkland Islands (Malvinas)',
			'French Guiana',
			'Guyana',
			'Paraguay',
			'Peru',
			'Suriname',
			'Uruguay',
			'Venezuela',
		],
	];


	public function run()
	{
		foreach ($this->data as $region => $countries)
		{
			$regionModel = \App\Region::create(['title' => $region]);
			foreach ($countries as $country)
			{
				$regionModel->countries()->save(\App\Country::make(['title' => $country]));
			}

		}
	}
}
