<?php

use Illuminate\Database\Seeder;

class TargetaudiencesTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	private $data = [
		'business',
		'investors',
		'consumers',
		'childs',
		'teenagers',
		'women',
		'men',
		'aged peoples',
		'drivers',
	];

	public function run()
	{
		foreach ($this->data as $title)
		{
			\App\Targetaudience::create([
				'title' => $title,
				'sort'  => 0
			]);
		}
	}
}
