<?php

use Illuminate\Database\Seeder;

class SectorIndustryTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	private $data = [
		'Energy'            => [
			'Refining',
			'Electricity',
			'Nuclear power',
			'Hydropower',
			'Solar power',
			'Wind power',
			'Geothermal power',
			'Bioenergy',
			'Firewood',
			'Fuel',
			'Storage & Transportation',
			'Equipment & Services',
			'Transmission & Distribution',
		],
		'Chemicals'         => [
			'Petrochemical',
			'Oil',
			'Lubricants',
			'Solvents',
			'Additives',
			'Explosives',
			'Fertilizers & Agricultural chemicals',
			'Aromatics',
			'Polymers',
			'Synthetic rubber',
			'Specialty Chemicals',
			'Industrial Chemicals',
		],
		'Utilities'         => [
			'Facility management',
			'Electricity',
			'Natural gas',
			'Water',
			'Waste',
			'Heating',
			'Internet & TV',
		],
		'Mining'            => [
			'Coal',
			'Uranium',
			'Petroleum',
			'Natural gas',
			'Exploration',
		],
		'Resources'         => [
			'Fishing',
			'Forestry',
			'Waste',
			'Water',
		],
		'Telecommunication' => [
			'Cable Networks',
		],
		'Transportation'    => [
			'Airport',
			'Airlines',
			'Railway',
			'Marine',
			'Logistics',
			'Trucking',
			'Pipeline',
			'Channels & Tunnels',
		],
		'Retail'            => [
			'Fuel station',
			'Drugstore',
			'Bazaar',
			'Boutique',
			'Chain store',
			'Concept store',
			'Discount store',
			'Minimarket',
			'Supermarket',
			'Hypermarket',
			'Internet store',
		],
		'Finance'           => [
			'Payment systems',
		],
		'IT'                => [
			'Technical standard',
		],
		'Military'          => [
			'Nuclear weapons',
		],
		'Education'         => [
			'Society',
		],
		'Vehicle'           => [
			'Automobiles',
			'Motorcycles',
		],
		'Drinks'            => [
			'Water',
			'Beer',
			'Vodka',
			'Wisky',
			'Energy drinks',
		],
		'Travel'            => [
			'Place branding',
		],
		'Media'             => [
			'TV Channel',
			'TV Program',
			'Social Network',
		],
		'Electronics'       => [
			'Phones',
		],
		'Infrastructure'    => [
			'Highways',
		],
		'Pharmaceuticals'   => [

		]
	];

	public function run()
	{
		foreach ($this->data as $sectorTitle => $sectorData)
		{
			$sector = \App\Sector::create(['title' => $sectorTitle]);
			foreach ($sectorData as $industry)
			{
				$sector->industries()->save(\App\Industry::make(['title' => $industry]));
			}
		}
	}
}
