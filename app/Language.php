<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Language extends Model
{
	protected $visible = [
		'id',
		'title'
	];

	public function symbolicfiles()
	{
		return $this->belongsToMany('App\Symbolicfile');
	}
}
