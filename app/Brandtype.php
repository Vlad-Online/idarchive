<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Brandtype extends Model
{
	protected $fillable = [
		'title'
	];
	protected $visible = [
		'title',
		'id'
	];

	public function brands()
	{
		return $this->hasMany('App\Brand');
	}
}
