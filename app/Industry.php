<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Industry extends Model
{
	protected $perPage = 100;

	protected $visible = [
		'id',
		'title'
	];

	public function brands()
	{
		return $this->belongsToMany('App\Brand');
	}

	public function sectors()
	{
		return $this->belongsToMany('App\Sector');
	}
}
