<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Year extends Model
{
	protected $visible = ['id', 'title'];

	public function symbolics()
	{
		return $this->belongsToMany('App\Symbolic');
	}
}
