<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Color extends Model
{
	protected $visible = [
		'id',
		'title',
		'code',
	];

	public function symbolicfiles()
	{
		return $this->belongsToMany('App\Symbolicfile');
	}
}
