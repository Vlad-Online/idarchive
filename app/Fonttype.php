<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Fonttype extends Model
{
	protected $visible = [
		'id',
		'title'
	];

	public function symbolicfiles()
	{
		return $this->belongsToMany('App\Symbolicfile');
	}
}
