<?php

namespace App\Console\Commands;

use App\Brand;
use App\Mail\LinksChecked;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class CheckLinks extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'links:check {link?}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Check brands site materials and designguides links';

	private $client;
	private $resultsArr = [];

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		$this->client = new Client();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		if ($this->argument('link'))
		{
			echo (string) $this->checkLink($this->argument('link')) . "\r\n";

			return;
		}
		Brand::with([
			'site',
			'symbolics.designguides',
			'symbolics.materials'
		])->chunk(100, function ($brands) {
			foreach ($brands as $brand)
			{
				$brandArr = [];

				$result = $brand->site ? $this->checkLink($brand->site->sitelink) : true;

				if (gettype($result) == 'string')
				{
					if (preg_match('#http[s]{0,1}\://([^/]*)#i', $brand->site->sitelink, $matches))
					{
						if (!stristr($result, $matches[1]))
						{
							$brandArr['site']['sitelink'] = $result;
							$brandArr['site']['oldLink']  = $brand->site->sitelink;
						}
					}
					else
					{
						$brandArr['site']['sitelink'] = $result;
						$brandArr['site']['oldLink']  = $brand->site->sitelink;
					}

				}
				elseif ($result == false)
				{
					$brandArr['site']['sitelink'] = '';
					$brandArr['site']['oldLink']  = $brand->site->sitelink;
				}

				foreach ($brand->symbolics as $symbolic)
				{
					$arr             = [];
					$designGuidesArr = $this->checkGuidesMaterials($symbolic->designguides);
					$materialsArr    = $this->checkGuidesMaterials($symbolic->materials);
					if (sizeof($designGuidesArr)) $arr['designguides'] = $designGuidesArr;
					if (sizeof($materialsArr)) $arr['materials'] = $materialsArr;
					if (sizeof($arr)) $brandArr['symbolics'][] = $arr;
				}

				if (sizeof($brandArr))
				{
					$brandArr           = array_merge($brandArr, [
						'id'    => $brand->id,
						'title' => $brand->title
					]);
					$this->resultsArr[] = $brandArr;
				}
			}

		});

		return Mail::to(config('mail.from.address'))->send(new LinksChecked($this->resultsArr));
	}

	public function checkLink($link, $withRedirects = false)
	{
		$options = [
			'allow_redirects' => $withRedirects ? [
				'max'             => 10,        // allow at most 10 redirects.
				'track_redirects' => true
			] : false,
			'verify'          => false,
			'headers'         => [
				'User-Agent'      => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36',
				'accept'          => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
				'accept-encoding' => 'gzip, deflate, br',
			]
		];

		try
		{
			$response = $this->client->get($link, $options);
			$code     = $response->getStatusCode();
		}
		catch (RequestException $e)
		{
			$code = 0;
		}

		if ($code >= 200 && $code < 300)
		{
			if ($withRedirects)
			{
				$redirects = $response->getHeader('X-Guzzle-Redirect-History');

				return array_pop($redirects);
			}

			return true;
		}
		elseif ($code >= 300 && $code < 400)
		{
			//TODO check new link
			return $this->checkLink($link, true);
		}
		else
		{
			return false;
		}
	}

	function checkGuidesMaterials($payloads)
	{
		$resultsArr = [];
		foreach ($payloads as $payload)
		{
			$result = $payload->link ? $this->checkLink($payload->link) : true;
			if ($result !== true)
			{
				$resultsArr[] = [
					'id'      => $payload->id,
					'link'    => $result,
					'oldLink' => $payload->link
				];

			}
		}

		return $resultsArr;

	}
}
