<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Logo;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	/*	public function index()
		{
			$brand = Brand::whereNotNull('main_page_image')->inRandomOrder()->first();
			//$path = json_decode($brand->main_page_image)[0]->download_link;
			$path = Storage::disk(config('voyager.storage.disk'))->url($brand->main_page_image);

			return view('layouts.idarchive', [
				'mainPageImageHref' => $path,
				'brandId'           => $brand->id,
				'brandTitle'        => $brand->title,
			]);
		}*/

	/*	public function getLogo()
		{
			$logo    = Logo::inRandomOrder()->first();
			$logoSrc = json_decode($logo->path)[0]->download_link;
			$logoSrc = Storage::disk(config('voyager.storage.disk'))->url($logoSrc);

			return response()->json($logoSrc);
		}*/

	public function getIndexData()
	{
		$brand = Brand::whereJsonLength('main_page_image', '>', 0)->inRandomOrder()->first();
		//$brand         = Brand::find(1);
		$brandImageSrc = json_decode($brand->main_page_image)[0]->download_link;
		$brandImageSrc = Storage::disk(config('voyager.storage.disk'))->url($brandImageSrc);

		$logo    = Logo::inRandomOrder()->first();
		$logoSrc = json_decode($logo->path)[0]->download_link;
		$logoSrc = Storage::disk(config('voyager.storage.disk'))->url($logoSrc);

		$user = Auth::guard('api')->user();
		if ($user)
		{
			$user->loadUserData();
		}

		return response()->json([
			'brandImageSrc' => $brandImageSrc,
			'brandId'       => $brand->id,
			'brandTitle'    => $brand->title,
			'logoSrc'       => $logoSrc,
			'recaptchaKey'  => config('recaptcha.public_key'),
			'user'          => $user
		]);
	}
}
