<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    use RegistersUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request)
    {
	    $token      = (string) Auth::guard('api')->getToken();
	    $expiration = Auth::guard('api')->getPayload()->get('exp');

	    return [
		    'token'      => $token,
		    'token_type' => 'bearer',
		    'expires_in' => $expiration - time(),
	    ];
    }

    protected function guard()
    {
	    return Auth::guard('api');
    }

	/**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
			'recaptchaResponse' => 'required|recaptcha',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
	 *
	 * @return \App\User
     */
    protected function create(array $data)
    {
		$user = User::create([
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

		$user->subscription()->create(['value' => 0]);

		return $user;
    }
}
