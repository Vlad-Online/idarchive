<?php

namespace App\Http\Controllers\Auth;

use App\Mail\UserAdded;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
	use AuthenticatesUsers;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest')->except('logout');
	}

	public function redirectToFacebook()
	{
		return Socialite::with('facebook')->stateless()->redirect();
	}

	public function handleFacebookCallback()
	{
		$user = Socialite::driver('facebook')->stateless()->user();

		return $this->loginSocialiteUser($user);
	}

	public function redirectToGoogle()
	{
		return Socialite::with('Google')->stateless()->redirect();
	}

	public function handleGoogleCallback()
	{
		$user = Socialite::driver('Google')->stateless()->user();

		return $this->loginSocialiteUser($user);
	}


	public function redirectToTwitter()
	{
		return Socialite::with('Twitter')->stateless()->redirect();
	}

	public function handleTwitterCallback()
	{
		$user = Socialite::driver('Twitter')->stateless()->user();

		return $this->loginSocialiteUser($user);
	}

	/**
	 * Attempt to log the user into the application.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return bool
	 */
	protected function attemptLogin(Request $request)
	{
		$token = Auth::guard('api')->attempt($this->credentials($request));
		if ($token)
		{
			Auth::guard('api')->setToken($token);

			return true;
		}

		return false;
	}

	protected function sendLoginResponse(Request $request)
	{
		$this->clearLoginAttempts($request);

		$token      = (string) Auth::guard('api')->getToken();
		$expiration = Auth::guard('api')->getPayload()->get('exp');

		return [
			'token'      => $token,
			'token_type' => 'bearer',
			'expires_in' => $expiration - time(),
		];
	}

	/**
	 * Log the user out of the application.
	 *
	 * @param  \Illuminate\Http\Request $request
	 */
	public function logout(Request $request)
	{
		Auth::guard('api')->logout();
	}

	public function authorizeFromEmail(Request $request, User $user)
	{
		if (!$request->hasValidSignature())
		{
			abort(401);
		}

		Auth::guard('api')->login($user, true);
		$token = (string) Auth::guard('api')->getToken();

		return redirect('/')->cookie('token', $token);
	}

	private function loginSocialiteUser($socialiteUser)
	{
		$user = User::where('email', $socialiteUser->getEmail())->first();
		if ($user)
		{
			Auth::guard('api')->login($user, true);
			$token = (string) Auth::guard('api')->getToken();

			return redirect()->route('index')->cookie('token', $token);
		}
		else
		{
			// generate random 10 char password from below chars
			$random   = str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890!$%^&!$%^&');
			$password = substr($random, 0, 10);

			$user = User::create([
				'name'     => $socialiteUser->getName(),
				'email'    => $socialiteUser->getEmail(),
				'password' => Hash::make($password)
			]);

			$user->subscription()->create([
				'value' => 0
			]);

			Mail::to($user)
				->queue(new UserAdded($user, $password));

			Auth::guard('api')->login($user, true);
			$token = (string) Auth::guard('api')->getToken();

			return redirect()->to('/profile/?showTariffPlansModal=1')->cookie('token', $token);
		}
	}
}
