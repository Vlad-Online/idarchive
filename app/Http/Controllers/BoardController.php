<?php

namespace App\Http\Controllers;

use App\Board;
use App\BoardColor;
use App\Logo;
use App\Symbolicfile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BoardController extends Controller
{
	public function getBoards(Request $request)
	{
		$boardsWith = [
			'color',
			'logo',
			'symbolicfiles.symbolic.brand',
			'symbolicfiles.user'
		];

		$boards = $this->getBoardOwner()->boards()
			->with($boardsWith)->orderBy('created_at')->get();

		return response()->json($boards);
	}

	public function addBoard(Request $request)
	{
		$this->getBoardOwner()->boards()->create([
			'title'          => htmlentities($request->title),
			'logo_id'        => Logo::inRandomOrder()->first()->id,
			'board_color_id' => BoardColor::inRandomOrder()->first()->id,
		]);

		return response('1');
	}

	public function pinToBoard(Board $board, Symbolicfile $symbolicfile)
	{
		if ($board && $symbolicfile && $board->user->id == $this->getBoardOwner()->id)
		{
			$board->symbolicfiles()->save($symbolicfile);

			return response()->json(1);
		}

		return response('', 404);
	}

	public function getColors()
	{
		return response()->json(BoardColor::all());
	}

	public function changeColor(Board $board, BoardColor $color)
	{
		if ($board && $color && $board->user->id == $this->getBoardOwner()->id)
		{
			$board->color()->associate($color);
			$board->save();

			return response()->json(1);
		}

		return response('', 404);
	}

	public function rename(Board $board, Request $request)
	{
		if ($board && $board->user->id == $this->getBoardOwner()->id)
		{
			$board->title = htmlentities($request->title);
			$board->save();

			return response()->json(1);
		}

		return response('', 404);
	}

	public function delete(Board $board)
	{
		if ($board && $board->user->id == $this->getBoardOwner()->id)
		{
			$board->symbolicfiles()->detach();
			$board->delete();

			return response()->json(1);
		}

		return response('', 404);
	}

	public function getBoard(Board $board)
	{
		if ($board && $board->user->id == $this->getBoardOwner()->id)
		{
			$board->load('symbolicfiles.symbolic.brand');
			$board->load('symbolicfiles.user');

			return response()->json($board);
		}

		return response('', 404);

	}

	public function unpin(Request $request, $id)
	{
		$board = $this->getBoardOwner()->boards()->whereHas('symbolicfiles', function ($query) use ($id) {
			$tableName = $query->getQuery()->from;
			$query->where($tableName . '.id', $id);
		})->first();
		if ($board)
		{
			$board->symbolicfiles()->detach($id);

			return response()->json(1);
		}

		return response('', 404);
	}

	public function pinAllToBoard(Request $request, Board $board)
	{
		if ($board && $board->user->id == $this->getBoardOwner()->id && $request->symbolicfiles)
		{
			$board->symbolicfiles()->attach($request->symbolicfiles);

			return response()->json(1);
		}

		return response('', 404);
	}

	/**
	 *
	 * @return User
	 *
	 * @since version
	 */
	private function getBoardOwner()
	{
		switch (Auth::user()->subscription->value)
		{
			case 2:
				return Auth::user()->subscription->parentSubscription->user;
				break;
			case 1:
			default:
				return Auth::user();
				break;
		}
	}
}
