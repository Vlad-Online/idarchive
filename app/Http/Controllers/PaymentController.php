<?php

namespace App\Http\Controllers;

use App\Payment;
use App\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use LiqPay;

class PaymentController extends Controller
{
	public function getLiqpayForm(Request $request, $tariffPlan = 1)
	{
		$user       = Auth::guard('api')->user();
		$payment    = $user->subscription->payment()->create();
		$liqpay     = new LiqPay(config('liqpay.public_key'), config('liqpay.private_key'));
		$verifyLink = URL::temporarySignedRoute(
			'verifyLiqpayPayment', now()->addWeek(), [
				'subscription_id' => $user->subscription->id,
				'value'           => $tariffPlan == 2 ? 3 : 1,
				'payment_id'      => $payment->id
			]
		);
		$html       = $liqpay->cnb_form(array(
			'action'      => 'pay',
			'amount'      => $tariffPlan == 2 ? '100' : '12',
			'currency'    => 'USD',
			'description' => 'idArchive account subscription',
			'order_id'    => (string) $payment->id,
			'version'     => '3',
			'result_url'  => config('app.url') . '/profile?tariffChanged',
			'sandbox'     => config('liqpay.sandbox'),
			'server_url'  => $verifyLink,
			'language'    => 'en'
		));

		Log::info('Verify link: ' . $verifyLink);

		return response()->json($html);
	}

	public function verifyLiqpayPayment(Request $request)
	{
		// Проверка подписи ссылки
		if (!$request->hasValidSignature())
		{
			Log::error('Link signature is not valid');
			abort(401);
		}

		$sign = base64_encode(sha1(
			config('liqpay.private_key') .
			$request->input('data') .
			config('liqpay.private_key')
			, 1));

		// Проверка подписи данных
		if ($sign != $request->input('signature'))
		{
			Log::error('Data signature is not valid');
			Log::error('Payment signature: ' . $request->input('signature'));
			Log::error('Data signature: ' . $sign);
			Log::error('Data: ' . $request->input('data'));
			abort(401);
		}

		$subscription = Subscription::find($request->input('subscription_id'));
		if ($subscription)
		{
			$subscription->value = $request->input('value');
			$subscription->save();
			$payment = Payment::find($request->input('payment_id'));
			if ($payment)
			{
				$payment->data = base64_decode($request->input('data'));
				$payment->save();
			}
			else
			{
				$subscription->payment()->create([
					'data' => base64_decode($request->input('data'))
				]);
			}

		}
		else
		{
			Log::error('Couldn\'t find subscription with id: ' . $request->input('subscription_id'));
			abort(401);
		}

		return response()->json('Ok, thank you');
	}
}
