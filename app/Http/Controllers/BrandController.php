<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Designguide;
use App\Material;
use App\Symbol;
use App\Symbolic;
use App\Symbolicfile;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class BrandController extends Controller
{

    public function ajaxSearch(Request $request)
    {
        $brands = Brand::where('title', $request->q)
            ->orWhereHas('brandnamealts', function ($query) use ($request) {
                $query->where('title', $request->q);
            })->with('symbolics');

        $user = Auth::guard('api')->user();
        if ($user) {
            $brands->with(
                [
                    'symbolics.symbolicfiles.user' => function ($query) use ($request) {
                        $query->where('user_id', Auth::guard('api')->user()->id);
                    }
                ]
            );
        } else {
            $brands->with('symbolics.symbolicfiles');
        }

        return response()->json($brands->get());
    }

    public function ajaxSearchTips(Request $request)
    {
        if (strlen($request->q) < 2) {
            return response()->json('', 422);
        }
        $brandsTable = DB::table('brands')->select('title')->where('title', 'like', $request->q.'%');
        $tips        = DB::table('brandnamealts')->select('title')->where('title', 'like', $request->q.'%')
            ->union($brandsTable)->orderBy('title')->limit(6)->get();

        return response()->json($tips);
    }

    public function ajaxBrand(Request $request)
    {
        $brand = Brand::with([
            'brandnamealts',
            'brandtype',
            'sectors',
            'industries',
            'country',
            'targetaudiences',
            'site',
            'predecessor',
            'parent',
            'symbolics'               => function ($query) {
                $query->orderBy('sort');
            },
            'symbolics.years',
            'symbolics.authors',
            'symbolics.designguides'  => function ($query) {
                $query->orderBy('sort');
            },
            'symbolics.designguides.filetype',
            'symbolics.materials'     => function ($query) {
                $query->orderBy('sort');
            },
            'symbolics.materials.filetype',
            'symbolics.symbolicfiles' => function ($query) {
                $query->orderBy('sort');
            },
            'symbolics.symbolicfiles.filetype',
            'symbolics.symbolicfiles.language',

        ])->find($request->id);

        if (!Auth::check()) {
            $brand->makeHidden('symbolics.symbolicfiles.file');
            $brand->makeHidden('symbolics.designguides.file');
            $brand->makeHidden('symbolics.materials.file');
        }

        return response()->json($brand);
    }

    public function getIds()
    {
        $ids = DB::table('brands')->select('id')->get();

        return response()->json($ids);
    }

    public function getCounters()
    {
        $data                  = [];
        $data['symbolicfiles'] = Symbolicfile::count();
        $data['designguides']  = Designguide::count();
        $data['brands']        = Brand::count();

        return response()->json($data);
    }

}
