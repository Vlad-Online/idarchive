<?php

namespace App\Http\Controllers;

use App\Author;
use App\Brand;
use App\Brandtype;
use App\Color;
use App\Country;
use App\Fonttype;
use App\Form;
use App\Industry;
use App\Language;
use App\Region;
use App\Sector;
use App\Simpleshape;
use App\Symbol;
use App\Symbolicfile;
use App\Symboltype;
use App\Symboltypeadd;
use App\Targetaudience;
use App\Technique;
use App\Visualkind;
use App\Year;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SearchAdvancedController extends Controller
{

	private $request, $queryBuilder;

	function ajaxParams()
	{
		$data = [
			'brandtypes'      => Brandtype::has('brands')->get(),
			'sectors'         => Sector::has('brands')->orderBy('title')->get(),
			'industries'      => Industry::has('brands')->orderBy('title')->get(),
			'regions'         => Region::has('countries.brands')->get(),
			'countries'       => Country::has('brands')->orderBy('title')->get(),
			'targetaudiences' => Targetaudience::has('brands')->get(),
			'years'           => Year::has('symbolics.brand')->orderBy('title', 'DESC')->get(),
			'languages'       => Language::has('symbolicfiles.symbolic.brand')->orderBy('title')->get(),
			'authors'         => Author::has('symbolics.brand')->orderBy('title')->get(),
			'symboltypes'     => Symboltype::has('symbolicfiles.symbolic.brand')->get(),
			'fonttypes'       => Fonttype::has('symbolicfiles.symbolic.brand')->get(),
			'colors'          => Color::has('symbolicfiles.symbolic.brand')->get(),
			'visualkinds'     => Visualkind::has('symbolicfiles.symbolic.brand')->get(),
			'techniques'      => Technique::has('symbolicfiles.symbolic.brand')->orderBy('title')->get(),
			'forms'           => Form::has('symbolicfiles.symbolic.brand')->orderBy('title')->get(),
			'simpleshapes'    => Simpleshape::has('symbolicfiles.symbolic.brand')->orderBy('title')->get(),
			'symbols'         => Symbol::has('symbolicfiles.symbolic.brand')->orderBy('title')->get(),
			'symboltypeadds'  => Symboltypeadd::has('symbolicfiles.symbolic.brand')->orderBy('sort')->get(),

		];

		return response()->json($data);
	}

	function ajaxCountries(Request $request)
	{
		if (!$request->id)
		{
			$countries = Country::has('brands')->orderBy('title')->get();
		}
		else
		{
			$ids       = explode(",", $request->id);
			$countries = Country::has('brands')->whereHas('region', function (Builder $query) use ($ids) {
				$query->whereIn('id', $ids);
			})->orderBy('title')->get();
		}

		return response()->json($countries);
	}

	function ajaxIndustries(Request $request)
	{
		if (!$request->id)
		{
			$industries = Industry::has('brands')->orderBy('title')->get();
		}
		else
		{
			$ids        = explode(",", $request->id);
			$industries = Industry::has('brands')->whereHas('sectors', function (Builder $query) use ($ids) {
				$query->whereIn('sectors.id', $ids);
			})->orderBy('title')->get();
		}

		return response()->json($industries);
	}


	function ajaxSearchAdvanced(Request $request)
	{
		$this->request      = $request;
		$this->queryBuilder = Symbolicfile::query();


		$this->addRelationQuery('brandtype', 'symbolic.brand.brandtype');
		$this->addRelationQuery('sector', 'symbolic.brand.sectors');
		$this->addRelationQuery('industry', 'symbolic.brand.industries');
		$this->addRelationQuery('region', 'symbolic.brand.country.region');
		$this->addRelationQuery('country', 'symbolic.brand.country');
		$this->addRelationQuery('targetaudience', 'symbolic.brand.targetaudiences');
		$this->addRelationQuery('year', 'symbolic.years');
		$this->addRelationQuery('language', 'language');
		$this->addRelationQuery('author', 'symbolic.authors');

		if ($this->request->status)
		{
			$this->queryBuilder->whereHas('symbolic', function ($query) {
				$tableName = $query->getQuery()->from;
				$query->whereIn($tableName . '.status', $this->request->status);
			});
		}

		if ($this->request->rating)
		{
			$this->queryBuilder->whereHas('symbolic', function ($query) {
				$tableName = $query->getQuery()->from;
				$query->where($tableName . '.rating', '>=', $this->request->rating);
			});
		}

		if ($this->request->designguide)
		{
			$this->queryBuilder->has('symbolic.designguides');
		}

		$this->addRelationQuery('symboltype', 'symboltype');
		$this->addRelationQuery('fonttypes', 'fonttypes');

		//Отбор по цветам
		if ($this->request->colors)
		{
			foreach ($this->request->colors as $colorId)
			{
				$this->queryBuilder->whereHas('colors', function ($query) use ($colorId) {
					$tableName = $query->getQuery()->from;
					$query->where($tableName . '.id', $colorId);
				});
			}
			if ($this->request->colorsexact)
			{
				$this->queryBuilder->whereDoesntHave('colors', function ($query) {
					$tableName = $query->getQuery()->from;
					$query->whereNotIn($tableName . '.id', $this->request->colors);
				});
			}
		}


		$this->addRelationQuery('visualkind', 'visualkind');
		$this->addRelationQuery('technique', 'techniques');
		$this->addRelationQuery('form', 'forms');
		$this->addRelationQuery('simpleshape', 'simpleshapes');
		$this->addRelationQuery('symbol', 'symbols');
		$this->addRelationQuery('symboltypeadd', 'symboltypeadds');

		$symbolicFiles = $this->queryBuilder->with(
			[
				'symbolic.brand',
				'user' => function ($query) use ($request) {
					if (Auth::guard('api')->user())
					{
						$query->where('user_id', Auth::guard('api')->user()->id);
					}
					else
					{
						$query->where('user_id', 0);
					}
				}
			]
		)->get();

		$symbolicFilesGrouped = $symbolicFiles->groupBy(function ($item, $key) {
			return $item->symbolic->id;
		});
		$symbolicFiles        = collect();
		$symbolicFilesGrouped->each(function ($group, $key) use ($symbolicFiles) {
			{
				$group = $group->sortBy('sort');
				$symbolicFiles->push($group->first());
			}
		});

		$subscriptionValue = Auth::guard('api')->user() ? (Auth::guard('api')->user()->subscription ? Auth::guard('api')->user()->subscription->value : 0) : 0;
		if (!$subscriptionValue)
		{
			$symbolicFiles = $symbolicFiles->splice(0, 20);
		}

		$symbolicFiles = $symbolicFiles->sortBy(function ($symbolicFile, $key) {
			//dd($symbolicFile->symbolic->brand->title);
			return $symbolicFile->symbolic->brand->title;
		})->values()->all();

		return response()->json([
			'results'     => $symbolicFiles,
			'currentPlan' => (Auth::guard('api')->user() && Auth::guard('api')->user()->subscription) ? Auth::guard('api')->user()->subscription->value : 0
		]);
	}

	public function ajaxSearchAdvancedCount(Request $request)
	{
		$this->request      = $request;
		$this->queryBuilder = Symbolicfile::query();


		$this->addRelationQuery('brandtype', 'symbolic.brand.brandtype');
		$this->addRelationQuery('sector', 'symbolic.brand.sectors');
		$this->addRelationQuery('industry', 'symbolic.brand.industries');
		$this->addRelationQuery('region', 'symbolic.brand.country.region');
		$this->addRelationQuery('country', 'symbolic.brand.country');
		$this->addRelationQuery('targetaudience', 'symbolic.brand.targetaudiences');
		$this->addRelationQuery('year', 'symbolic.years');
		$this->addRelationQuery('language', 'language');
		$this->addRelationQuery('author', 'symbolic.authors');

		if ($this->request->status)
		{
			$this->queryBuilder->whereHas('symbolic', function ($query) {
				$tableName = $query->getQuery()->from;
				$query->whereIn($tableName . '.status', $this->request->status);
			});
		}

		if ($this->request->rating)
		{
			$this->queryBuilder->whereHas('symbolic', function ($query) {
				$tableName = $query->getQuery()->from;
				$query->where($tableName . '.rating', '>=', $this->request->rating);
			});
		}

		if ($this->request->designguide)
		{
			$this->queryBuilder->has('symbolic.designguides');
		}

		$this->addRelationQuery('symboltype', 'symboltype');
		$this->addRelationQuery('fonttypes', 'fonttypes');

		//Отбор по цветам
		if ($this->request->colors)
		{
			foreach ($this->request->colors as $colorId)
			{
				$this->queryBuilder->whereHas('colors', function ($query) use ($colorId) {
					$tableName = $query->getQuery()->from;
					$query->where($tableName . '.id', $colorId);
				});
			}
			if ($this->request->colorsexact)
			{
				$this->queryBuilder->whereDoesntHave('colors', function ($query) {
					$tableName = $query->getQuery()->from;
					$query->whereNotIn($tableName . '.id', $this->request->colors);
				});
			}
		}


		$this->addRelationQuery('visualkind', 'visualkind');
		$this->addRelationQuery('technique', 'techniques');
		$this->addRelationQuery('form', 'forms');
		$this->addRelationQuery('simpleshape', 'simpleshapes');
		$this->addRelationQuery('symbol', 'symbols');
		$this->addRelationQuery('symboltypeadd', 'symboltypeadds');

		$symbolicFiles = $this->queryBuilder->get();

		$symbolicFilesGrouped = $symbolicFiles->groupBy(function ($item, $key) {
			return $item->symbolic->id;
		});
		$symbolicFiles        = collect();
		$symbolicFilesGrouped->each(function ($group, $key) use ($symbolicFiles) {
			{
				$group = $group->sortBy('sort');
				$symbolicFiles->push($group->first());
			}
		});

		return response()->json($symbolicFilesGrouped->count());
	}

	private function addRelationQuery($requestKey, $relationName)
	{
		if ($this->request->$requestKey)
		{
			$this->queryBuilder->whereHas($relationName, function ($query) use ($requestKey) {
				$tableName = $query->getQuery()->from;
				$query->whereIn($tableName . '.id', $this->request->$requestKey);
			});
		}
	}

}
