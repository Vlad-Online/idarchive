<?php

namespace App\Http\Controllers;

use App\Designguide;
use App\Material;
use App\Symbolicfile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class DownloadController extends Controller
{
	public function downloadDesignguide($id)
	{
		$designGuide = Designguide::find($id);

		return $this->download($designGuide->file);
	}

	public function downloadMaterial($id)
	{
		$material = Material::find($id);

		return $this->download($material->file);
	}

	public function downloadSymbolicfile($id)
	{
		$symbolicFile = Symbolicfile::find($id);

		$symbolicFile->user()->syncWithoutDetaching(Auth::user()->id);
		$symbolicFile->user()->updateExistingPivot(Auth::user()->id, []);

		return $this->download($symbolicFile->file);
	}

	private function download($file)
	{
		if ($file)
		{
			$data     = json_decode($file);
			$filePath = $data[0]->download_link;
			$fileName = $data[0]->original_name;

			return Storage::disk('public')->download($filePath, $fileName);
		}
		else
		{
			return response('Not found', 404);
		}
	}

}
