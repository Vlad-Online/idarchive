<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class SymbolicfileController extends FileController
{
    protected $path = 'symbolicfiles';
}
