<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class FileController extends Controller
{
    protected $path = '';

    public function uploadImage(Request $request)
    {
        $validated = $request->validate([
            'image' => 'file'
        ]);

        $file = $validated['image'];
        $path = $this->path.DIRECTORY_SEPARATOR.date('F').date('Y');

        $path = $request->file('image')->storeAs($path, $this->generateFileName($file, $path),
            config('voyager.storage.disk'));

        $fileParams = [
            [
                'download_link' => $path,
                'original_name' => $request->file('image')->getClientOriginalName()
            ]
        ];

        return response()->json($fileParams);
    }

    protected function generateFileName($file, $path)
    {
        $fileName      = Str::random();
        $fileExtension = $file->getClientOriginalExtension();

        while (Storage::disk(config('voyager.storage.disk'))->exists($path.DIRECTORY_SEPARATOR.$fileName.'.'.$fileExtension)) {
            $fileName = Str::random();
        }

        return $fileName.'.'.$fileExtension;
    }

}
