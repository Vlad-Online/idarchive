<?php

namespace App\Http\Controllers\Admin;

use App\Author;
use App\Brand;
use App\Brandtype;
use App\Color;
use App\Country;
use App\Designguide;
use App\Filetype;
use App\Fonttype;
use App\Form;
use App\Industry;
use App\Language;
use App\Material;
use App\Region;
use App\Sector;
use App\Simpleshape;
use App\Site;
use App\Symbol;
use App\Symbolic;
use App\Symbolicfile;
use App\Symboltype;
use App\Symboltypeadd;
use App\Targetaudience;
use App\Technique;
use App\Visualkind;
use App\Year;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class BrandController extends Controller
{

    protected $limit = 50;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'id'                                                 => 'sometimes|required|exists:brands,id',
            'title'                                              => 'required|max:50',
            'brandnamealts.*.title'                              => 'string|max:50',
            'brandtype.title'                                    => 'required|exists:brandtypes,title',
            'country.title'                                      => 'required|exists:countries,title',
            'site.sitelink'                                      => 'nullable|url|max:255',
            'site.title'                                         => 'nullable|string|max:50',
            'targetaudiences.*.title'                            => 'exists:targetaudiences,title',
            'predecessor.*.id'                                   => 'exists:brands,id',
            'parent.*.id'                                        => 'exists:brands,id',
            'sectors.*.title'                                    => 'exists:sectors,title',
            'industries.*.title'                                 => 'exists:industries,title',
            'main_page_image'                                    => 'nullable|string|json|max:512',
            'symbolics.*.id'                                     => 'sometimes|required|exists:symbolics,id',
            'symbolics.*.status'                                 => 'required|boolean',
            'symbolics.*.sort'                                   => 'integer',
            'symbolics.*.rating'                                 => 'required|between:1,5',
            'symbolics.*.years.*.title'                          => 'required|integer|exists:years,title',
            'symbolics.*.authors.*.title'                        => 'required|string|exists:authors,title',
            'symbolics.*.designguides.*.id'                      => 'sometimes|integer|exists:designguides,id',
            'symbolics.*.designguides.*.title'                   => 'required|string|max:100',
            'symbolics.*.designguides.*.link'                    => 'nullable|url|max:1024',
            'symbolics.*.designguides.*.sort'                    => 'integer',
            'symbolics.*.designguides.*.file'                    => 'nullable|string|max:1024',
            'symbolics.*.designguides.*.filetype.title'          => 'string|exists:filetypes,title',
            'symbolics.*.materials.*.id'                         => 'sometimes|integer',
            'symbolics.*.materials.*.title'                      => 'required|string|max:100',
            'symbolics.*.materials.*.link'                       => 'nullable|url|max:1024',
            'symbolics.*.materials.*.sort'                       => 'integer',
            'symbolics.*.materials.*.file'                       => 'nullable|string|max:1024',
            'symbolics.*.materials.*.filetype.title'             => 'string|exists:filetypes,title',
            'symbolics.*.symbolicfiles.*.id'                     => 'sometimes|required|exists:symbolicfiles,id',
            'symbolics.*.symbolicfiles.*.title'                  => 'required|string|max:255',
            'symbolics.*.symbolicfiles.*.file'                   => 'required|json|max:512',
            'symbolics.*.symbolicfiles.*.fileprev'               => 'required|json|max:512',
            'symbolics.*.symbolicfiles.*.sort'                   => 'integer',
            'symbolics.*.symbolicfiles.*.language.*.title'       => 'required|string|exists:languages,title',
            'symbolics.*.symbolicfiles.*.symboltype.title'       => 'required|string|exists:symboltypes,title',
            'symbolics.*.symbolicfiles.*.symboltypeadds.*.title' => 'required|string|exists:symboltypeadds,title',
            'symbolics.*.symbolicfiles.*.fonttypes.*.title'      => 'required|string|exists:fonttypes,title',
            'symbolics.*.symbolicfiles.*.colors.*.title'         => 'required|string|exists:colors,title',
            'symbolics.*.symbolicfiles.*.visualkind.title'       => 'required|string|exists:visualkinds,title',
            'symbolics.*.symbolicfiles.*.techniques.*.title'     => 'required|string|exists:techniques,title',
            'symbolics.*.symbolicfiles.*.forms.*.title'          => 'required|string|exists:forms,title',
            'symbolics.*.symbolicfiles.*.simpleshapes.*.title'   => 'required|string|exists:simpleshapes,title',
            'symbolics.*.symbolicfiles.*.symbols.*.title'        => 'required|string|exists:symbols,title',
            'symbolics.*.symbolicfiles.*.filetype.title'         => 'required|string|exists:filetypes,title',
        ]);

        $data = [
            'title'           => $validatedData['title'],
            'brandtype_id'    => $this->getModelId($validatedData['brandtype']['title'], 'Brandtype'),
            'region_id'       => Country::where('title', $validatedData['country']['title'])->first()->region->id,
            'country_id'      => $this->getModelId($validatedData['country']['title'], 'Country'),
            'main_page_image' => $validatedData['main_page_image'] ?? null
        ];

        if (isset($validatedData['id'])) {
            $brand = Brand::find($validatedData['id']);
            if ($brand->main_page_image != $data['main_page_image']) {
                $path = json_decode($brand->main_page_image);
                if (isset($path[0]->download_link)) {
                    $path = $path[0]->download_link;
                    Storage::disk(config('voyager.storage.disk'))->delete($path);
                }
            }
            $brand->update($data);
        } else {
            $brand = Brand::create($data);
        }

        if ($validatedData['site']['sitelink']) {
            $brand->site()->associate(Site::firstOrCreate([
                'title'    => $validatedData['site']['title'] ?? '',
                'sitelink' => $validatedData['site']['sitelink']
            ]))->save();
        }

        $brand->brandnamealts()->delete();

        if (isset($validatedData['brandnamealts'])) {
            $brand->brandnamealts()->createMany($validatedData['brandnamealts']);
        }

        $this->syncRelation($validatedData, 'targetaudiences', 'Targetaudience', $brand);
        $this->syncRelationByIds($validatedData, 'predecessor', 'Brand', $brand);
        $this->syncRelationByIds($validatedData, 'parent', 'Brand', $brand);
        $this->syncRelation($validatedData, 'sectors', 'Sector', $brand);
        $this->syncRelation($validatedData, 'industries', 'Industry', $brand);


        // Удаляем ненужные символики
        $this->deleteNotExists('symbolics', $validatedData, $brand);

        // Сохранение символик
        if (isset($validatedData['symbolics'])) {
            foreach ($validatedData['symbolics'] as $symbolicData) {

                $data = [
                    'status' => $symbolicData['status'],
                    'sort'   => $symbolicData['sort'],
                    'rating' => $symbolicData['rating']
                ];

                if (isset($symbolicData['id'])) {
                    $symbolic = Symbolic::find($symbolicData['id']);
                    $symbolic->update($data);

                    // Удаляем ненужные дизайнгайды
                    $this->deleteNotExists('designguides', $symbolicData, $symbolic);
                    // Удаляем ненужные материалы
                    $this->deleteNotExists('materials', $symbolicData, $symbolic);
                    // Удаляем ненужные символики
                    $this->deleteNotExists('symbolicfiles', $symbolicData, $symbolic);
                } else {
                    $symbolic = $brand->symbolics()->create($data);
                }

                $this->syncRelation($symbolicData, 'years', 'Year', $symbolic);
                $this->syncRelation($symbolicData, 'authors', 'Author', $symbolic);

                // Сохранение дизайнгайдов
                if (isset($symbolicData['designguides'])) {
                    foreach ($symbolicData['designguides'] as $designguideData) {
                        $data = [
                            'title'       => $designguideData['title'],
                            'link'        => $designguideData['link'],
                            'sort'        => $designguideData['sort'],
                            'filetype_id' => Filetype::where('title',
                                $designguideData['filetype']['title'])->first()->id,
                            'file'        => $designguideData['file'] ?? ''
                        ];
                        if (isset($designguideData['id'])) {
                            $designguide = Designguide::find($designguideData['id']);
                            if ($designguide->file && $designguideData['file'] != $designguide->file) {
                                $path = json_decode($designguide->file);
                                $path = $path[0]->download_link;
                                Storage::disk(config('voyager.storage.disk'))->delete($path);
                            }
                            $designguide->update($data);
                        } else {
                            $symbolic->designguides()->create($data);
                        }
                    }
                }


                // Сохранение материалов
                if (isset($symbolicData['materials'])) {
                    foreach ($symbolicData['materials'] as $materialData) {
                        $data = [
                            'title'       => $materialData['title'],
                            'link'        => $materialData['link'],
                            'sort'        => $materialData['sort'],
                            'filetype_id' => Filetype::where('title', $materialData['filetype']['title'])->first()->id,
                            'file'        => $materialData['file'] ?? ''
                        ];
                        if (isset($materialData['id'])) {
                            $material = Material::find($materialData['id']);
                            if ($material->file && $materialData['file'] != $material->file) {
                                $path = json_decode($material->file);
                                $path = $path[0]->download_link;
                                Storage::disk(config('voyager.storage.disk'))->delete($path);
                            }

                            $material->update($data);
                        } else {
                            $symbolic->materials()->create($data);
                        }
                    }
                }

                // Сохранение файлов символик
                if (isset($symbolicData['symbolicfiles'])) {
                    foreach ($symbolicData['symbolicfiles'] as $symbolicfileData) {
                        $data = [
                            'title'         => $symbolicfileData['title'],
                            'file'          => $symbolicfileData['file'],
                            'fileprev'      => $symbolicfileData['fileprev'],
                            'sort'          => $symbolicfileData['sort'],
                            'symboltype_id' => Symboltype::where('title',
                                $symbolicfileData['symboltype']['title'])->first()->id,
                            'visualkind_id' => Visualkind::where('title',
                                $symbolicfileData['visualkind']['title'])->first()->id,
                            'filetype_id'   => Filetype::where('title',
                                $symbolicfileData['filetype']['title'])->first()->id,
                        ];
                        if (isset($symbolicfileData['id'])) {
                            $symbolicfile = Symbolicfile::find($symbolicfileData['id']);
                            if ($symbolicfileData['file'] != $symbolicfile->file) {
                                $path = json_decode($symbolicfile->file);
                                $path = $path[0]->download_link;
                                Storage::disk(config('voyager.storage.disk'))->delete($path);
                            }

                            if ($symbolicfileData['fileprev'] != $symbolicfile->fileprev) {
                                $path = json_decode($symbolicfile->fileprev);
                                $path = $path[0]->download_link;
                                Storage::disk(config('voyager.storage.disk'))->delete($path);
                            }

                            $symbolicfile->update($data);
                        } else {
                            $symbolicfile = $symbolic->symbolicfiles()->create($data);
                        }

                        $this->syncRelation($symbolicfileData, 'language', 'Language', $symbolicfile);
                        $this->syncRelation($symbolicfileData, 'symboltypeadds', 'Symboltypeadd', $symbolicfile);
                        $this->syncRelation($symbolicfileData, 'fonttypes', 'Fonttype', $symbolicfile);
                        $this->syncRelation($symbolicfileData, 'colors', 'Color', $symbolicfile);
                        $this->syncRelation($symbolicfileData, 'techniques', 'Technique', $symbolicfile);
                        $this->syncRelation($symbolicfileData, 'forms', 'Form', $symbolicfile);
                        $this->syncRelation($symbolicfileData, 'simpleshapes', 'Simpleshape', $symbolicfile);
                        $this->syncRelation($symbolicfileData, 'symbols', 'Symbol', $symbolicfile);
                    }
                }
            }

        }

        return redirect()->route('admin_get_brand', ['brand' => $brand->id]);
    }

    private function getModelId($title, $modelName)
    {
        return ('App\\'.ucfirst($modelName))::where('title', $title)->first()->id;
    }

    private function syncRelation($validatedData, $relationName, $modelName, $modelToSync)
    {
        if (!isset($validatedData[$relationName])) {
            return $modelToSync->$relationName()->detach();
        }
        $titles = array_column($validatedData[$relationName], 'title');
        $ids    = ('App\\'.$modelName)::whereIn('title', $titles)->get();

        return $modelToSync->$relationName()->sync($ids);
    }

    private function syncRelationByIds($validatedData, $relationName, $modelName, $modelToSync)
    {
        if (!isset($validatedData[$relationName])) {
            return $modelToSync->$relationName()->detach();
        }
        $ids = array_column($validatedData[$relationName], 'id');
        //$ids    = ('App\\'.$modelName)::whereIn('title', $titles)->get();

        return $modelToSync->$relationName()->sync($ids);
    }

    private function deleteNotExists($relationName, $data, $model)
    {
        if (!isset($data[$relationName])) {
            $ids = [];
        } else {
            $ids = array_column($data[$relationName], 'id');
        }

        return $model->$relationName()->whereNotIn('id', $ids)->get()->each(function ($relationModel) {
            $relationModel->delete();
        });
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Brand  $brand
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        $brand->load([
            'brandnamealts',
            'brandtype',
            'sectors',
            'industries',
            'country',
            'targetaudiences',
            'site',
            'predecessor',
            'parent',
            'symbolics.years',
            'symbolics.authors',
            'symbolics.designguides.filetype',
            'symbolics.materials.filetype',
            'symbolics.symbolicfiles.language',
            'symbolics.symbolicfiles.symboltype',
            'symbolics.symbolicfiles.symboltypeadds',
            'symbolics.symbolicfiles.fonttypes',
            'symbolics.symbolicfiles.colors',
            'symbolics.symbolicfiles.visualkind',
            'symbolics.symbolicfiles.techniques',
            'symbolics.symbolicfiles.forms',
            'symbolics.symbolicfiles.simpleshapes',
            'symbolics.symbolicfiles.symbols',
            'symbolics.symbolicfiles.filetype',
            'symbolics.years',
        ]);

        $brand->predecessor->each(function ($item) {
            $item->title = $item->title." ({$item->id})";
        });

        $brand->parent->each(function ($item) {
            $item->title = $item->title." ({$item->id})";
        });

        return response()->json($brand);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Brand  $brand
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Brand  $brand
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Request  $request
     * @param  \App\Brand  $brand
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Request $request, Brand $brand)
    {
        $brand->brandnamealts()->delete();
        $brand->sectors()->detach();
        $brand->industries()->detach();
        $brand->targetaudiences()->detach();
        $brand->site()->delete();
        $brand->predecessor()->detach();
        $brand->parent()->detach();

        $brand->symbolics()->each(function (Symbolic $symbolic) {
            $symbolic->years()->detach();
            $symbolic->authors()->detach();

            $symbolic->designguides()->each(function (Designguide $designguide) {
                $designguide->delete();
            });

            $symbolic->materials()->each(function (Material $material) {
                $material->delete();
            });

            $symbolic->symbolicfiles()->each(function (Symbolicfile $symbolicfile) {
                $symbolicfile->delete();
            });
        });

        $brand->delete();

        return response()->json(1);
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Builder
     *
     * @since version
     */
    private function getBuilder()
    {
        return Brand::query()->with([
            'brandtype',
            'country',
            'site'
        ])->orderByDesc('id')
            ->limit($this->limit);
    }

    /**
     * Search method for admin interface
     *
     * @param  Request  $request
     *
     *
     * @return \Illuminate\Http\JsonResponse
     * @since version
     */

    public function search(Request $request)
    {

        if ($request->filled('q')) {
            $q = $request->input('q');
            if (is_numeric($q)) {
                // Поиск по id бренда
                return response()->json($this->getBuilder()->where('id', $q)->get());
            } else {
                // Поиск по названиям бренда
                $builder = $this->getBuilder();
                $builder->where('title', $q)
                    ->orWhereHas('brandnamealts', function ($query) use ($q) {
                        $query->where('title', $q);
                    });
                if ($builder->count()) {
                    return response()->json($builder->get());
                } else {
                    // Поиск по стране
                    $builder = $this->getBuilder();
                    $builder->whereHas('country', function ($query) use ($q) {
                        $query->where('title', $q);
                    });
                    if ($builder->count()) {
                        return response()->json($builder->get());
                    } else {
                        // Поиск по адресу сайта
                        $builder = $this->getBuilder();
                        $builder->whereHas('site', function ($query) use ($q) {
                            $query->where('sitelink', 'like', "%{$q}%");
                        });
                        if ($builder->count()) {
                            return response()->json($builder->get());
                        } else {
                            return response()->json([]);
                        }
                    }
                }
            }
        } else {
            return response()->json($this->getBuilder()->get());
        }
    }

    public function ajaxParams()
    {
        $data = [
            'brandtypes'      => Brandtype::all(),
            'sectors'         => Sector::orderBy('title')->get(),
            'industries'      => Industry::orderBy('title')->get(),
            'regions'         => Region::has('countries.brands')->get(),
            'countries'       => Country::orderBy('title')->get(),
            'targetaudiences' => Targetaudience::all(),
            'years'           => Year::orderBy('title', 'DESC')->get(),
            'languages'       => Language::orderBy('title')->get(),
            'authors'         => Author::orderBy('title')->get(),
            'symboltypes'     => Symboltype::all(),
            'fonttypes'       => Fonttype::all(),
            'colors'          => Color::all(),
            'visualkinds'     => Visualkind::all(),
            'techniques'      => Technique::orderBy('title')->get(),
            'forms'           => Form::orderBy('title')->get(),
            'simpleshapes'    => Simpleshape::orderBy('title')->get(),
            'symbols'         => Symbol::orderBy('title')->get(),
            'symboltypeadds'  => Symboltypeadd::orderBy('sort')->get(),
            'filetypes'       => Filetype::all(),

        ];

        return response()->json($data);
    }

    public function uploadImage(Request $request)
    {
        $validated = $request->validate([
            'image' => 'file'
        ]);
        $path      = $request->file('image')->store('brands', config('voyager.storage.disk'));

        $fileParams = [
            [
                'download_link' => $path,
                'original_name' => $request->file('image')->getClientOriginalName()
            ]
        ];

        return response()->json($fileParams);
    }

    public function ajaxSearchTips(Request $request)
    {
        if (strlen($request->q) < 2) {
            return response()->json('', 422);
        }
        $brandsTable = DB::table('brands')->select(DB::raw('id, concat(title, " (",id,")") as title'))->where('title',
            'like', $request->q.'%');
        $tips        = DB::table('brandnamealts')->select(DB::raw('brand_id as id, concat(title, " (",brand_id,")") as title'))->where('title',
            'like', $request->q.'%')
            ->union($brandsTable)->orderBy('title')->limit(6)->get();

        return response()->json($tips);
    }


}
