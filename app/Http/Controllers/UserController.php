<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function getUserData(Request $request)
    {
        $user = Auth::guard('api')->user()->loadUserData();

        return response()->json($user);
    }

    public function saveUserData(Request $request)
    {

        $validatedData = $request->validate([
            'name'     => 'required|string|max:255',
            'email'    => 'required|string|email|max:255',
            'password' => 'sometimes|required|string|min:6',
        ]);

        $user = Auth::guard('api')->user();

        if (isset($validatedData['password']) && $validatedData['password']) {
            $validatedData['password'] = Hash::make($validatedData['password']);
        }
        $user->fill($validatedData)->save();

        return response()->json($user);
    }

    public function deleteAccount(Request $request)
    {
        $user = Auth::guard('api')->user();
        $user->boards()->delete();
        if ($user->subscription) {
            $user->subscription->childSubscriptions()->delete();
            $user->subscription->delete();
        }

        $user->delete();

        return response()->json([
            'message' => 'User successfully deleted'
        ]);
    }
}
