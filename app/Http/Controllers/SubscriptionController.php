<?php

namespace App\Http\Controllers;

use App\Mail\UserAdded;
use App\Subscription;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class SubscriptionController extends Controller
{
	public function saveCorporateUser(Request $request)
	{
		$validatedData = $request->validate([
			'email'           => 'required|string|email|max:255',
			'subscription_id' => 'sometimes|required|integer|exists:subscriptions,id'
		]);

		if (User::where('email', $validatedData['email'])->first()) return response()->json([
			'message' => 'User already exists'
		], 422);

		$childSubscriptions = Auth::guard('api')->user()->subscription->childSubscriptions;
		$subscription       = null;
		if (isset($validatedData['subscription_id']))
		{
			$subscription = $childSubscriptions->firstWhere('id', $validatedData['subscription_id']);
		}

		if ($childSubscriptions->count() >= 9 && !$subscription) return response()->json([
			'message' => 'Users limit reached'
		], 422);

		// generate random 10 char password from below chars
		$random   = str_shuffle('abcdefghjklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ234567890!$%^&!$%^&');
		$password = substr($random, 0, 10);

		$userToSave           = new User();
		$userToSave->password = Hash::make($password);
		$userToSave->email    = $validatedData['email']; // grab the email from the posted form
		$userToSave->save();

		Mail::to($userToSave)
			->queue(new UserAdded($userToSave, $password));

		if ($subscription)
		{
			$oldUser               = $subscription->user;
			$subscription->user_id = $userToSave->id;
			$subscription->save();

			$oldUser->subscription()->create([
				'value' => 0
			]);
		}
		else
		{
			$userToSave->subscription()->create([
				'value'                  => 2,
				'parent_subscription_id' => Auth::guard('api')->user()->subscription->id
			]);
		}

		return response()->json([
			'message' => 'User successfully saved',
		]);

	}

	public function unsubscribeCorporateUser(Subscription $subscription)
	{
		if ($subscription->parentSubscription->user->is(Auth::user()))
		{
			$subscription->parent_subscription_id = null;
			$subscription->value                  = 0;
			$subscription->save();

			return response()->json([
				'message' => 'User successfully deleted'
			]);
		}
		else
		{
			return response()->json([
				'message' => 'User not found'
			], 422);
		}
	}
}
