<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckSubscription
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Closure                 $next
	 *
	 * @return mixed
	 */
	public function handle($request, Closure $next, $subscriptionVal = 1)
	{
		$user         = Auth::guard('api')->user();
		$subscription = $user ? $user->subscription : null;


		if ($user && $subscription && $subscription->value >= $subscriptionVal)
			// Если подписка есть
			switch ($subscriptionVal)
			{
				case 2:
					if ($subscription->parentSubscription->updated_at->addYear()->gt(Carbon::now())) return $next($request);
					break;
				case 1:
				default:
					if ($subscription->updated_at->addYear()->gt(Carbon::now())) return $next($request);
					break;
			}

		return response(['error' => 'Your subscription is not valid'], 404);
	}

}
