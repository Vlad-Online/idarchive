<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $role = 'admin')
    {
        if (Auth::check() && (Auth::guard('api')->user()->hasRole($role) || Auth::guard('api')->user()->hasRole('admin'))) {
            return $next($request);
        }

        return response(['error' => 'Access denied'], 404);
    }
}
