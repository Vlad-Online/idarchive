<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class Symbolicfile extends Model
{
    use Notifiable;

    protected $perPage = 100;

    protected $fillable = [
        'title',
        'file',
        'fileprev',
        'sort',
        'symboltype_id',
        'visualkind_id',
        'filetype_id'
    ];

    protected $visible = [
        'id',
        'fileprev',
        'filetype',
        'language',
        'title',
        'symbolic',
        'user',
    ];

    public function __construct(array $attributes = [])
    {
        if (Auth::check() && (Auth::guard('api')->user()->hasRole('mediamanager') || Auth::guard('api')->user()->hasRole('admin'))) {
            $this->makeVisible([
                'sort',
                'symboltype',
                'symboltypeadds',
                'file',
                'visualkind',
                'fonttypes',
                'techniques',
                'forms',
                'simpleshapes',
                'symbols',
                'colors'
            ]);
        }
        parent::__construct($attributes);
    }

    public function language()
    {
        return $this->belongsToMany('App\Language');
    }

    public function symboltype()
    {
        return $this->belongsTo('App\Symboltype');
    }

    public function symboltypeadds()
    {
        return $this->belongsToMany('App\Symboltypeadd');
    }

    public function fonttypes()
    {
        return $this->belongsToMany('App\Fonttype');
    }

    public function colors()
    {
        return $this->belongsToMany('App\Color');
    }

    public function visualkind()
    {
        return $this->belongsTo('App\Visualkind');
    }

    public function techniques()
    {
        return $this->belongsToMany('App\Technique');
    }

    public function forms()
    {
        return $this->belongsToMany('App\Form');
    }

    public function simpleshapes()
    {
        return $this->belongsToMany('App\Simpleshape');
    }

    public function symbols()
    {
        return $this->belongsToMany('App\Symbol');
    }

    public function filetype()
    {
        return $this->belongsTo('App\Filetype');
    }

    public function symbolic()
    {
        return $this->belongsTo('App\Symbolic');
    }

    // Relation for user downloaded symbolicfiles
    public function user()
    {
        return $this->belongsToMany('App\User')->withTimestamps();
    }

    public function delete()
    {
        $this->language()->detach();
        $this->symboltypeadds()->detach();
        $this->fonttypes()->detach();
        $this->colors()->detach();
        $this->techniques()->detach();
        $this->forms()->detach();
        $this->simpleshapes()->detach();
        $this->symbols()->detach();
        $this->user()->detach();

        if ($this->file) {
            $path = json_decode($this->file);
            $path = $path[0]->download_link;
            Storage::disk(config('voyager.storage.disk'))->delete($path);
        }

        if ($this->fileprev) {
            $path = json_decode($this->fileprev);
            $path = $path[0]->download_link;
            Storage::disk(config('voyager.storage.disk'))->delete($path);
        }

        return parent::delete();
    }
}
