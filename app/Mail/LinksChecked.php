<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class LinksChecked extends Mailable
{
	use Queueable, SerializesModels;

	private $brands = [];

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct($brands)
	{
		$this->brands = $brands;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		return $this->view('emails.checkLinksReport')
			->with(['brands' => $this->brands])
			->subject('Links checking report');
	}
}
