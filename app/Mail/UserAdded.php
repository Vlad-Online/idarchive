<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\URL;

class UserAdded extends Mailable
{
	use Queueable, SerializesModels;

	protected $user, $password;

	/**
	 * Create a new message instance.
	 *
	 * @return void
	 */
	public function __construct(User $user, $password)
	{
		$this->user     = $user;
		$this->password = $password;
	}

	/**
	 * Build the message.
	 *
	 * @return $this
	 */
	public function build()
	{
		return $this->view('emails.corporateUserCreated')
			->with([
				'login'    => $this->user->email,
				'link'     => URL::temporarySignedRoute(
					'authorizeFromEmail', now()->addMonths(1), ['user' => $this->user->id]
				),
				'password' => $this->password
			])
			->subject('Your idArchive account credentials');
	}
}
