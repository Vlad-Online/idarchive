<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Targetaudience extends Model
{
	protected $visible = [
		'id',
		'title'
	];

	public function brands()
	{
		return $this->belongsToMany('App\Brand');
	}
}
