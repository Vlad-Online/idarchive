<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Board extends Model
{
	protected $fillable = [
		'title',
		'logo_id',
		'board_color_id'
	];

	protected $visible = [
		'id',
		'title',
		'color',
		'logo',
		'symbolicfiles',
		'created_at'
	];

	public function color()
	{
		return $this->belongsTo('App\BoardColor', 'board_color_id');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function symbolicfiles()
	{
		return $this->belongsToMany('App\Symbolicfile');
	}

	public function logo()
	{
		return $this->belongsTo('App\Logo');
	}

}
