<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Brandnamealt extends Model
{
    protected $fillable = [
        'title'
    ];
}
