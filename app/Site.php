<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Site extends Model
{
    protected $fillable = [
        'title',
        'sitelink'
    ];

	protected $visible = [
        'title',
        'sitelink'
    ];
}
