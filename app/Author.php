<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Author extends Model
{
	protected $fillable = [
		'title'
	];

	protected $visible = [
		'id',
		'title'
	];

	public function symbolics()
	{
		return $this->belongsToMany('App\Symbolic');
	}
}
