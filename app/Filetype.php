<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Filetype extends Model
{
    protected $visible = [
        'title'
    ];

    protected $fillable = [
        'title',
        'link',
        'sort',
        'filetype_id',
        'file'
    ];
}
