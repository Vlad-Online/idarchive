<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
	protected $fillable = [
		'value',
		'parent_subscription_id',
	];

	protected $visible = [
		'id',
		'value',
		'childSubscriptions',
		'user'
	];

	public function parentSubscription()
	{
		return $this->belongsTo('App\Subscription', 'parent_subscription_id');
	}

	public function childSubscriptions()
	{
		return $this->hasMany('App\Subscription', 'parent_subscription_id');
	}

	public function user()
	{
		return $this->belongsTo('App\User');
	}

	public function payment()
	{
		return $this->hasMany('App\Payment');
	}

}
