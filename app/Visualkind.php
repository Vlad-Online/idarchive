<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Visualkind extends Model
{
	protected $visible = [
		'id',
		'title'
	];

	public function symbolicfiles()
	{
		return $this->hasMany('App\Symbolicfile');
	}
}
