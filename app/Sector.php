<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Sector extends Model
{
	protected $perPage = 100;

	protected $fillable = [
		'title'
	];

	protected $visible = [
		'id',
		'title'
	];

	public function industries()
	{
		return $this->belongsToMany('App\Industry');
	}

	public function brands()
	{
		return $this->belongsToMany('App\Brand');
	}

}
