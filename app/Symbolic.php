<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Symbolic extends Model
{
	protected $perPage = 100;

	protected $fillable = [
		'status',
		'sort',
        'rating'
	];

	protected $visible = [
		'id',
		'status',
		'rating',
		'symbolicfiles',
		'years',
		'authors',
		'designguides',
		'materials',
		'brand',
        'sort'
	];

	public function years()
	{
		return $this->belongsToMany('App\Year');
	}

	public function authors()
	{
		return $this->belongsToMany('App\Author');
	}

	public function designguides()
	{
		return $this->hasMany('App\Designguide');
	}

	public function materials()
	{
		return $this->hasMany('App\Material');
	}

	public function symbolicfiles()
	{
		return $this->hasMany('App\Symbolicfile')->orderBy('sort');
	}

	public function brand()
	{
		return $this->belongsTo('App\Brand');
	}

	public function delete()
    {
        $this->years()->detach();
        $this->authors()->detach();
        $this->designguides()->delete();
        $this->materials()->delete();
        $this->symbolicfiles()->delete();

        return parent::delete();
    }
}
