<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


class Designguide extends Model
{
    protected $perPage = 100;

    protected $fillable = [
        'title',
        'link',
        'sort',
        'filetype_id',
        'file'
    ];

    protected $visible = [
        'id',
        'title',
        'link',
        'filetype'
    ];

    public function __construct(array $attributes = [])
    {
        if (Auth::check() && (Auth::guard('api')->user()->hasRole('mediamanager') || Auth::guard('api')->user()->hasRole('admin'))) {
            $this->makeVisible([
                'sort',
                'file'
            ]);
        }
        parent::__construct($attributes);
    }

    public function filetype()
    {
        return $this->belongsTo('App\Filetype');
    }

    public function delete()
    {
        if ($this->file) {
            $path = json_decode($this->file);
            $path = $path[0]->download_link;
            Storage::disk(config('voyager.storage.disk'))->delete($path);
        }

        return parent::delete();
    }
}
