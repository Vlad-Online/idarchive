<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Country extends Model
{
	protected $visible = [
		'id',
		'title',
		'region'
	];

	public function region()
	{
		return $this->belongsTo('App\Region');
	}

	public function brands()
	{
		return $this->hasMany('App\Brand');
	}
}
