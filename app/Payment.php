<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
	protected $fillable = [
		'data'
	];

	public function subscription()
	{
		return $this->belongsTo('App\Subscription');
	}
}
