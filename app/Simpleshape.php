<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Simpleshape extends Model
{
	protected $visible = [
		'id',
		'title'
	];

	public function symbolicfiles()
	{
		return $this->belongsToMany('App\Symbolicfile');
	}
}
