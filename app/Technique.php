<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Technique extends Model
{
	protected $visible = [
		'id',
		'title'
	];

	public function symbolicfiles()
	{
		return $this->belongsToMany('App\Symbolicfile');
	}
}
