<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Brand extends Model
{
	protected $perPage = 100;

	protected $fillable = [
		'title',
        'brandtype_id',
        'region_id',
        'country_id',
        'site_id',
        'main_page_image',
	];

	protected $visible = [
		'id',
		'title',
		'symbolics',
		'brandtype',
		'sectors',
		'industries',
		'region',
		'country',
		'site',
		'predecessor',
		'parent',
        'main_page_image',
        'brandnamealts',
        'targetaudiences'
	];

	public function brandnamealts()
	{
		return $this->hasMany('App\Brandnamealt');
	}

	public function brandtype()
	{
		return $this->belongsTo('App\Brandtype');
	}

	public function sectors()
	{
		return $this->belongsToMany('App\Sector');
	}

	public function industries()
	{
		return $this->belongsToMany('App\Industry');
	}

	public function country()
	{
		return $this->belongsTo('App\Country');
	}

	public function targetaudiences()
	{
		return $this->belongsToMany('App\Targetaudience');
	}

	public function site()
	{
		return $this->belongsTo('App\Site');
	}

	public function predecessor()
	{
		return $this->belongsToMany('App\Brand', 'brand_predecessor', 'brand_id', 'predecessor_id');
	}

	public function parent()
	{
		return $this->belongsToMany('App\Brand', 'brand_parent', 'brand_id', 'parent_id');
	}

	public function symbolics()
	{
		return $this->hasMany('App\Symbolic')->orderBy('sort');
	}

}
