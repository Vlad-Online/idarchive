<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Region extends Model
{
	protected $fillable = [
		'title'
	];

	protected $visible = [
		'id',
		'title'
	];

	public function countries()
	{
		return $this->hasMany('App\Country');
	}
}
