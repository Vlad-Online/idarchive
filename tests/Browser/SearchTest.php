<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class SearchTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     * @group search
     * @return void
     */
    public function testSearchTips()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
	            ->type('search', 're')
	            ->pause(2000)
	            ->assertPresent('.search-tip')
	            ->click('.search-tip> li:first-child')
	            ->waitForText('Search result for')
	            ->waitFor('.gr-tb', 1)
	            ->assertPresent('.gr-tb');
        });
    }
}
