<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     * @group login
     * @return void
     */
    public function testLogin()
    {
    	$user = factory(User::class)->create();

        $this->browse(function (Browser $browser) use ($user){
            $browser->visit('/')
	            ->click('.h-logo')
	            ->pause(500)
	            ->assertSee('Sign in')
	            ->clickLink('Sign in')
	            ->waitFor('#md-sign-in')
	            ->type('email', $user->email)
	            ->type('password', 'secret')
	            ->click('button[type="submit"]')
	            ->pause(1500)
	            ->assertSee('You are logged in')
	            ->assertSee('Profile');
        });
    }
}
