export default ({store, redirect}) => {
    if (store.getters['auth/role'] == 'admin' || store.getters['auth/role'] == 'mediamanager') {
        return;
    }
    return redirect('/');
}
