require('dotenv').config()
import webpack from 'webpack'
const axios = require('axios')

const polyfills = [
    'Promise',
    'Object.assign',
    'Object.values',
    'Array.prototype.find',
    'Array.prototype.findIndex',
    'Array.prototype.includes',
    'String.prototype.includes',
    'String.prototype.startsWith',
    'String.prototype.endsWith'
]

module.exports = {
    // mode: 'spa',

    srcDir: __dirname,

    env: {
        apiUrl: process.env.APP_URL || 'http://api.laravel-nuxt.test',
        appName: process.env.APP_NAME || 'Laravel-Nuxt',
        appLocale: process.env.APP_LOCALE || 'en',
        githubAuth: !!process.env.GITHUB_CLIENT_ID
    },

    head: {
        title: process.env.APP_NAME,
        titleTemplate: '%s | ' + process.env.APP_NAME,
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description', name: 'description', content: 'IdArchive'},
            {name: "msapplication-TileColor", content: "#d50000"},
            {name: "theme-color", content: "#ffffff"}
        ],
        link: [
            {rel: 'icon', type: 'image/svg+xml', sizes: 'any', href: '/images/favicon/favicon.svg'},
            {rel: "apple-touch-icon", sizes: "180x180", href: "/images/favicon/apple-touch-icon.png"},
            {rel: "icon", type: "image/png", sizes: "32x32", href: "/images/favicon/favicon-32x32.png"},
            {rel: "icon", type: "image/png", sizes: "16x16", href: "/images/favicon/favicon-16x16.png"},
            {rel: "manifest", href: "/images/favicon/site.webmanifest"},
            {rel: "mask-icon", href: "/images/favicon/safari-pinned-tab.svg", color: "#d50000"},
            {rel: "stylesheet", href: "https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons"}
        ],
        script: [
            {src: `https://cdn.polyfill.io/v2/polyfill.min.js?features=${polyfills.join(',')}`},
            {
                src: "https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit",
                async: true,
                defer: true
            }
        ]
    },

    loading: {color: '#007bff'},

    router: {
        middleware: ['locale', 'check-auth']
    },

    css: [
        'vue-js-modal/dist/styles.css',
        '~assets/css/all.css',
        /*{src: '~assets/sass/app.scss', lang: 'scss'}*/
    ],

    plugins: [
        '~components/global',
        '~plugins/i18n',
        '~plugins/vform',
        '~plugins/axios',
        // '~plugins/fontawesome',
        '~plugins/nuxt-client-init',
        {src: '~plugins/bootstrap', ssr: false},
        '~plugins/lazyload',
        '~plugins/vue-js-modal',
/*        '~plugins/vue-material'*/
        '~plugins/vuetify'
    ],

    modules: [
        '@nuxtjs/router',
        '~/modules/spa',
        ['@nuxtjs/google-analytics', {
            id: 'UA-134271071-1'
        }],
        ['vue-scrollto/nuxt', {
            offset: -32,
        }],
        '@nuxtjs/sitemap',
        '@nuxtjs/axios',
    ],

    build: {
        extractCSS: true,
        plugins: [
            new webpack.ProvidePlugin({
                '_': 'lodash'
            })
        ],
        html: {
            minify: {
                minifyCSS: true,
                minifyJS: true,
                trimCustomFragments: true,
            }
        }
    },
    sitemap: {
        path: '/sitemap.xml',
        hostname: 'https://idarchive.app',
        cacheTime: 1000 * 60 * 60,
        gzip: true,
        exclude: [
            '/api/**',
            '/board/**',
            '/boards',
            '/profile',
            '/admin/**'
        ],
        routes() {
            let staticRoutes = [
                '/',
                '/about',
                '/termsofuse',
                '/privacypolicy',
                '/privacydmca',
            ];
            return axios.get(process.env.APP_URL + '/api/ajax_brand_ids')
                .then(({data}) => {
                    let routes =  data.map(brand => '/brand/' + brand.id)
                    return staticRoutes.concat(routes);
                })
        }
    }
}

