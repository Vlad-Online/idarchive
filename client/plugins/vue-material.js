import Vue from 'vue'
import {
    MdChips,
    MdAutocomplete,
    MdField,
    MdMenu,
    MdButton,
    MdList
} from 'vue-material/dist/components'

//import VueMaterial from 'vue-material'
//import 'vue-material/dist/vue-material.min.css'
Vue.use(MdChips)
Vue.use(MdAutocomplete)
Vue.use(MdField)
Vue.use(MdMenu)
Vue.use(MdButton)
Vue.use(MdList)


