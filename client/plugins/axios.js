//import axios from 'axios'
import swal from 'sweetalert2'

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'

export default function ({$axios, app, store, redirect}) {

    $axios.defaults.baseURL = process.env.apiUrl

    if (process.server) {
        $axios.onRequest(request => {
            request.metadata = {startTime: new Date()}
            return request
        })

        $axios.onResponse(response => {
            response.config.metadata.endTime = new Date()
            response.duration = response.config.metadata.endTime - response.config.metadata.startTime
            console.log(response.config.metadata.endTime.getHours() + ':' + response.config.metadata.endTime.getMinutes() + ':' + response.config.metadata.endTime.getSeconds() + ' ' + response.duration + 'ms url: ' + response.config.url)
            return response
        })

        $axios.onError(error => {
            error.config.metadata.endTime = new Date();
            error.duration = error.config.metadata.endTime - error.config.metadata.startTime;
            console.log(error.config.metadata.endTime.getHours() + ':' + error.config.metadata.endTime.getMinutes() + ':' + error.config.metadata.endTime.getSeconds() + ' ' + error.duration + 'ms Error url: ' + error.config.url)
            return Promise.reject(error)
        })

        return
    }

    // Request interceptor
    $axios.onRequest(request => {
        request.baseURL = process.env.apiUrl

        const token = store.getters['auth/token']

        if (token) {
            request.headers.common['Authorization'] = `Bearer ${token}`
        }

        const locale = store.getters['lang/locale']
        if (locale) {
            request.headers.common['Accept-Language'] = locale
        }

        return request
    })

    // Response interceptor
    $axios.onError(error => {
        const {status} = error.response || {}

        //console.log(JSON.stringify(error.response.data));

/*        if (status >= 500) {
            swal.fire({
                type: 'error',
                title: app.i18n.t('error_alert_title'),
                text: app.i18n.t('error_alert_text'),
                reverseButtons: true,
                confirmButtonText: app.i18n.t('ok'),
                cancelButtonText: app.i18n.t('cancel')
            })
        }*/

        if (status === 401 && store.getters['auth/check']) {
/*            swal({
                type: 'warning',
                title: app.i18n.t('token_expired_alert_title'),
                text: app.i18n.t('token_expired_alert_text'),
                reverseButtons: true,
                confirmButtonText: app.i18n.t('ok'),
                cancelButtonText: app.i18n.t('cancel')
            }).then(() => {*/
                store.commit('auth/LOGOUT')
                redirect({name: 'index'})
            //})
        }

        return Promise.reject(error)
    })
}
