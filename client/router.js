import Vue from 'vue'
import Router from 'vue-router'
import {scrollBehavior} from '~/utils'

Vue.use(Router)

//const Home = () => import('~/pages/home').then(m => m.default || m)
//const Welcome = () => import('~/pages/welcome').then(m => m.default || m)
const Index = () => import('~/pages/index').then(m => m.default || m)
const About = () => import('~/pages/aboutus').then(m => m.default || m)
const TermsOfUse = () => import('~/pages/termsOfUse').then(m => m.default || m)
const PrivacyPolicy = () => import('~/pages/privacyPolicy').then(m => m.default || m)
const PrivacyDMCA = () => import('~/pages/privacyDmca').then(m => m.default || m)
const Boards = () => import('~/pages/boards').then(m => m.default || m)
const Board = () => import('~/pages/board').then(m => m.default || m)
const Profile = () => import('~/pages/profile').then(m => m.default || m)
const Search = () => import('~/pages/search').then(m => m.default || m)
const SearchAdvanced = () => import('~/pages/searchAdvanced').then(m => m.default || m)
const Brand = () => import('~/pages/brand').then(m => m.default || m)
const AdminBrands = () => import('~/pages/admin/brands').then(m => m.default || m)
const AdminBrand = () => import('~/pages/admin/brand').then(m => m.default || m)

const Login = () => import('~/pages/auth/login').then(m => m.default || m)
const Register = () => import('~/pages/auth/register').then(m => m.default || m)
const PasswordReset = () => import('~/pages/auth/password/reset').then(m => m.default || m)
const PasswordRequest = () => import('~/pages/auth/password/email').then(m => m.default || m)

const Settings = () => import('~/pages/settings/index').then(m => m.default || m)
const SettingsProfile = () => import('~/pages/settings/profile').then(m => m.default || m)
const SettingsPassword = () => import('~/pages/settings/password').then(m => m.default || m)

const routes = [
    {path: '/', name: 'index', component: Index},
    {path: '/about', name: 'about', component: About},
    {path: '/termsofuse', name: 'termsofuse', component: TermsOfUse},
    {path: '/privacypolicy', name: 'privacypolicy', component: PrivacyPolicy},
    {path: '/privacydmca', name: 'privacyDmca', component: PrivacyDMCA},
    {path: '/boards', name: 'boards', component: Boards},
    {
        path: '/board/:board_id',
        component: Board,
        name: 'board',
        meta: {title: 'Board'}
    },
    {path: '/profile', name: 'profile', component: Profile},
    {path: '/search_advanced', name: 'search_advanced', component: SearchAdvanced},
    {
        path: '/brand/:brand_id', name: 'brand', component: Brand, children: [
            {
                path: 'symbolicfile/:symbolicfile_id',
                component: Brand,
                name: 'brandSymbolicfile'
            }
        ]
    },
    {path: '/search/:query', name: 'search', component: Search},

/*    {path: '/login', name: 'login', component: Login},
    {path: '/register', name: 'register', component: Register},*/
    {path: '/password/reset', name: 'password.request', component: PasswordRequest},
    {path: '/password/reset/:token', name: 'password.reset', component: PasswordReset},
    {path: '/editor/brands', name: 'admin.brands', component: AdminBrands},
    {path: '/editor/brand/:id', name: 'admin.brand', component: AdminBrand},

/*    {
        path: '/settings',
        component: Settings,
        children: [
            {path: '', redirect: {name: 'settings.profile'}},
            {path: 'profile', name: 'settings.profile', component: SettingsProfile},
            {path: 'password', name: 'settings.password', component: SettingsPassword}
        ]
    }*/
]

export function createRouter() {
    return new Router({
        routes,
        scrollBehavior,
        mode: 'history'
    })
}
