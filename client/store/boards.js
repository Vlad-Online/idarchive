//import axios from 'axios'

// state
export const state = () => ({
    boards: [],
})

// getters
export const getters = {
    isPinned: state => symbolicfileId => {
        return state.boards.find(board => {
            return board.symbolicfiles.find(symbolicfile => symbolicfile.id === symbolicfileId)
        })
    },
}

// mutations
export const mutations = {
    setBoards(state, data) {
        state.boards = data;
    },
    setBoardColorSelector(state, data) {
        state.boards.find(board => board.id == data.id).showColorSelector = data.value;
    },
    setBoardTitleInput(state, data) {
        state.boards.find(board => board.id == data.id).showTitleInput = data.value;
    },
    sortBoards(state, sortFunction) {
        state.boards.sort(sortFunction);
    }

}

// actions
export const actions = {
    async updateBoards({commit}) {
        let {data} = await this.$axios.get('/api/ajax_get_boards')
        if (Array.isArray(data)) {
            data.forEach(board => {
                board.showColorSelector = false;
                board.showTitleInput = false;
            });
            commit('setBoards', data);
        }
        //context.commit('setLoading', false);

    },
/*    sortBoards(context, sortFunction) {
        let boards = context.state.boards;
        boards.sort(sortFunction);
        context.commit('setBoards', boards);
    },*/
    unpinSymbolicfile({dispatch}, sId) {
        this.$axios.get('/api/ajax_unpin/' + sId)
            .then(response => {
                dispatch('updateBoards');
            })

    },
}
