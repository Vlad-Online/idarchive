//import axios from 'axios'

// state
export const state = () => ({
    logoSrc: '',
    showMessage: false,
    message: '',
    recaptchaKey: '',
    brandId: '',
    brandImageSrc: '',
    brandTitle: ''
})

// getters
export const getters = {}

// mutations
export const mutations = {
    SET_MESSAGE(state, data) {
        state.message = data;
    },
    SHOW_MESSAGE(state) {
        state.showMessage = true;
    },
    HIDE_MESSAGE(state) {
        state.showMessage = false;
    },
    SET_INDEX_DATA(state, data) {
        state = Object.assign(state, data);
    }
}

// actions
export const actions = {
    getIndexData({commit, rootState, error}) {
        return this.$axios.get('/api/get_index_data').then(({data}) => {
            commit('SET_INDEX_DATA', data)
            commit('auth/FETCH_USER_SUCCESS', data.user, {root: true});
        }).catch(e => {
            console.log(e);
        })
    },
    setMessage({commit}, data) {
        commit('SET_MESSAGE', data);
        commit('SHOW_MESSAGE');
        setTimeout(() => commit('HIDE_MESSAGE'), 3000);
    }
}
