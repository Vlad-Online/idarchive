import Cookies from 'js-cookie'
import {cookieFromRequest} from '~/utils'

export const actions = {
    async nuxtServerInit({commit, dispatch}, {req}) {
        const token = cookieFromRequest(req, 'token')
        //console.log('Server cookie: ' + token)
        if (token) {
            commit('auth/SET_TOKEN', token)
        }

        const locale = cookieFromRequest(req, 'locale')
        if (locale) {
            commit('lang/SET_LOCALE', {locale})
        }
        await dispatch('mainpage/getIndexData');
    },

    nuxtClientInit({commit, dispatch}) {
        const token = Cookies.get('token')
        //console.log('Client cookie: ' + token)
        if (token) {
            commit('auth/SET_TOKEN', token)
        }

        const locale = Cookies.get('locale')
        if (locale) {
            commit('lang/SET_LOCALE', {locale})
        }
    }
}
