//import axios from 'axios'

// state
export const state = () => ({
    searchAdvancedParamValuesDefault: {
        status: [1]
    },
    searchAdvancedParamValues: {
        status: [1]
    },
})

// getters
export const getters = {
    searchAdvancedParamValues(state) {
        return Object.keys(state.searchAdvancedParamValues).length ? state.searchAdvancedParamValues : state.searchAdvancedParamValuesDefault;
    },
}

// mutations
export const mutations = {
    setSearchAdvancedParamValueState(state, data) {
        state.searchAdvancedParamValues = Object.assign({}, state.searchAdvancedParamValues, {[data.key]: data.value});
    },
    resetSearchAdvanced(state) {
        state.searchAdvancedParamValues = state.searchAdvancedParamValuesDefault;
    },
}

// actions
export const actions = {
    setSearchAdvancedParamValue({commit}, data) {
        commit('setSearchAdvancedParamValueState', data)
    },
    resetSearchAdvanced({commit}) {
        commit('resetSearchAdvanced');
    },
    /*setMenuButtonSrc({commit}) {
        return axios.get('/api/get_logo').then(({data}) => {
            console.log(data);
            commit('SET_MENU_BUTTON_SRC', data)
        })
    },
    setMessage({context}, data) {
        context.commit('SET_MESSAGE', data);
        context.commit('SHOW_MESSAGE');
        setTimeout(() => context.commit('HIDE_MESSAGE'), 3000);
    }*/
}
