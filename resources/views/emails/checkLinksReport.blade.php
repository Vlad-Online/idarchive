@extends('emails.templateClear')

@section('body')
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
        <tbody>
        <tr>
            <td align="left">
                <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                    @php
                        //xdebug_break();
                    @endphp
                    @foreach($brands as $brand)
                        <tr>
                            <td>
                                {{$brand['title']}}
                            </td>
                            <td>
                            </td>
                            <td>
                                Old link
                            </td>
                            <td>
                                New link
                            </td>
                        </tr>
                        @if(isset($brand['site']))
                            <tr>
                                <td>

                                </td>
                                <td>
                                    Sitelink
                                </td>
                                <td>
                                    <a href="{{$brand['site']['oldLink']}}"
                                       target="_blank">{{$brand['site']['oldLink']}}</a>
                                </td>
                                <td>
                                    <a href="{{$brand['site']['sitelink']}}"
                                       target="_blank">{{$brand['site']['sitelink']}}</a>
                                </td>
                            </tr>
                        @endif
                        @if(isset($brand['symbolics']))
                            @foreach($brand['symbolics'] as $symbolic)
                                @php
                                    xdebug_break();
                                @endphp
                                @if(isset($symbolic['designguides']))
                                    @foreach($symbolic['designguides'] as $designguide)
                                        <tr>
                                            <td>

                                            </td>
                                            <td>
                                                Designguide
                                            </td>
                                            <td>
                                                <a href="{{$designguide['oldLink']}}"
                                                   target="_blank">{{$designguide['oldLink']}}</a>
                                            </td>
                                            <td>
                                                <a href="{{$designguide['link']}}"
                                                   target="_blank">{{$designguide['link']}}</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                @if(isset($symbolic['materials']))
                                    @foreach($symbolic['materials'] as $material)
                                        <tr>
                                            <td>

                                            </td>
                                            <td>
                                                Material
                                            </td>
                                            <td>
                                                <a href="{{$material['oldLink']}}"
                                                   target="_blank">{{$material['oldLink']}}</a>
                                            </td>
                                            <td>
                                                <a href="{{$material['link']}}"
                                                   target="_blank">{{$material['link']}}</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
@endsection