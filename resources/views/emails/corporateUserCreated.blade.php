@extends('emails.template')

@section('body')
    <p>Hi there,</p>
    <p>Here is your credentials to our service:</p>
    <p>Login: {{$login}}</p>
    <p>Password: {{$password}}</p>
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn btn-primary">
        <tbody>
        <tr>
            <td align="left">
                <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>
                        <td><a href="{{ $link }}" target="_blank">Click here to login</a></td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    <p>Good luck!</p>
@endsection