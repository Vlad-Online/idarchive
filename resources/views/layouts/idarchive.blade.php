<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- favicons -->
    <link rel="icon" type="image/svg+xml" sizes="any" href="/images/favicon/favicon.svg">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="/images/favicon/site.webmanifest">
    <link rel="mask-icon" href="/images/favicon/safari-pinned-tab.svg" color="#d50000">
    <meta name="msapplication-TileColor" content="#d50000">
    <meta name="theme-color" content="#ffffff">


    <!-- facebook -->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="idarchive.app">
    <meta property="og:title" content="idArchive">
    <meta property="og:description" content="The source of the original brand identity. For inspiration and experience, trend tracking and knowledge of history.">
    <meta property="og:url" content="{{route('index')}}">
    <meta property="og:locale" content="en_us">
    <meta property="og:image" content="/images/sn_card.jpg">
    <!-- google+ -->
    <meta itemprop="headline" content="idArchive"/>
    <meta itemprop="description" content="The source of the original brand identity. For inspiration and experience, trend tracking and knowledge of history."/>
    <meta itemprop="image" content="/images/sn_card.jpg"/>
    <!-- twitter -->
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:site" content="idarchive.app"/>
    <meta name="twitter:title" content="idArchive">
    <meta name="twitter:description" content="The source of the original brand identity. For inspiration and experience, trend tracking and knowledge of history."/>
    <meta name="twitter:creator" content="idArchive"/>
    <meta name="twitter:image:src" content="/images/sn_card.jpg"/>
    <meta name="twitter:domain" content="idarchive.app"/>

    @auth
        <meta name="auth" content="">
    @endauth
    <script type="text/javascript">
        window.brandId={{$brandId}};
        window.brandTitle="{{$brandTitle}}";
        window.backgroundImageSrc="{{$mainPageImageHref}}";
    </script>
    <meta name="google-site-verification" content="D9LPl-FBIwY9iqB_85xZfeplLH7zMoutnDhtV-ozBvI" />
    <script src="https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit" async defer></script>
    <script type="text/javascript">
        window.recaptchaKey = "{{config('recaptcha.public_key')}}";
    </script>
</head>
<body>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    @csrf
</form>
<div id="app">
    <router-view></router-view>
</div>
</body>
</html>
