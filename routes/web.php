<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'admin'], function () {
	Voyager::routes();
});

Auth::routes();

//Route::get('/{any}', 'HomeController@index')->where('any', '.*');

Route::get('{path?}', function () {
	return redirect('/');
	//return file_get_contents(public_path('_nuxt/index.html'));
})->where('path', '(.*)')->name('index');
