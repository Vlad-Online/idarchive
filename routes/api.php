<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('api')->group(function () {
    // Получение картинки с логотипом
    Route::get('get_logo', 'HomeController@getLogo');

    Route::get('recaptcha_key', 'HomeController@getRecaptchaKey');

    // Получение изображения бренда и ссылки на главной
    Route::get('get_index_data', 'HomeController@getIndexData');

    // Получение id брендов для генерации sitemap
    Route::get('ajax_brand_ids', 'BrandController@getIds');

    // Получение счетчиков для страницы about
    Route::get('get_counters', 'BrandController@getCounters');

    // Получение списков параметров для расширенного поиска
    Route::get('ajax_search_advanced_params', 'SearchAdvancedController@ajaxParams');
    Route::get('ajax_search_advanced_countries', 'SearchAdvancedController@ajaxCountries');
    Route::get('ajax_search_advanced_industries', 'SearchAdvancedController@ajaxIndustries');
    Route::post('ajax_search_advanced', 'SearchAdvancedController@ajaxSearchAdvanced');
    Route::post('ajax_search_advanced_count', 'SearchAdvancedController@ajaxSearchAdvancedCount');

    // Поиск по названию
    Route::get('ajax_search', 'BrandController@ajaxSearch');
    Route::get('ajax_search_tips', 'BrandController@ajaxSearchTips');
    Route::get('ajax_brand', 'BrandController@ajaxBrand');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('logout', 'Auth\LoginController@logout');

        Route::get('user', 'UserController@getUserData');

        //Route::patch('settings/profile', 'Settings\ProfileController@update');
        //Route::patch('settings/password', 'Settings\PasswordController@update');

        // Удаление аккаунта
        Route::get('/ajax_delete_account', 'UserController@deleteAccount');

        // Получение формы для оплаты LiqPay
        Route::get('/ajax_get_liqpay_form/{tariffPlan}', 'PaymentController@getLiqpayForm');

        // Сохранение профиля пользователя
        Route::post('/ajax_save_user', 'UserController@saveUserData');

        // Маршруты, доступные по подписке
        Route::middleware('subscription')->group(function () {
            Route::get('/ajax_pin_to_board/{board}/{symbolicfile}', 'BoardController@pinToBoard');
            Route::post('/ajax_pin_all_to_board/{board}', 'BoardController@pinAllToBoard');
            Route::post('/create_board', 'BoardController@create');
            Route::get('/ajax_board/{board}', 'BoardController@getBoard');

            Route::get('/ajax_get_boards', 'BoardController@getBoards');
            Route::post('/ajax_add_board', 'BoardController@addBoard');
            Route::get('/ajax_get_colors', 'BoardController@getColors');
            Route::get('/ajax_change_board_color/{board}/{color}', 'BoardController@changeColor');
            Route::post('/ajax_rename_board/{board}', 'BoardController@rename');
            Route::get('/ajax_delete_board/{board}', 'BoardController@delete');
            Route::get('/ajax_unpin/{id}', 'BoardController@unpin');
        });

        // Маршруты, доступные по  корпоративной подписке мастер пользователю
        Route::middleware('subscription:3')->group(function () {
            Route::post('/ajax_save_corporate_user', 'SubscriptionController@saveCorporateUser');
            Route::get('/ajax_unsubscribe_corporate_user/{subscription}',
                'SubscriptionController@unsubscribeCorporateUser');
        });

        Route::middleware('role:mediamanager')->group(function () {
            // Поиск для редактора брендов
            Route::get('ajax_search_admin', 'Admin\BrandController@search');
            // Удаление бренда
            Route::get('ajax_delete_brand/{brand}', 'Admin\BrandController@destroy');
            Route::get('ajax_get_brand/{brand}', 'Admin\BrandController@show')->name('admin_get_brand');
            Route::get('ajax_get_brand_params', 'Admin\BrandController@ajaxParams');
            Route::post('ajax_save_brand', 'Admin\BrandController@store');
            Route::post('ajax_upload_main_image', 'Admin\BrandController@uploadImage');
            Route::post('symbolicfile/ajax_upload_image', 'Admin\SymbolicfileController@uploadImage');
            Route::post('material/ajax_upload_image', 'Admin\MaterialController@uploadImage');
            Route::post('designguide/ajax_upload_image', 'Admin\DesignguideController@uploadImage');
            Route::get('admin/ajax_search_tips', 'Admin\BrandController@ajaxSearchTips');

        });
    });

    Route::group(['middleware' => 'guest:api'], function () {
        Route::post('login', 'Auth\LoginController@login');
        Route::post('register', 'Auth\RegisterController@register');

        Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
        Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    });

    // Авторизация по ссылке из Email
    Route::get('/authorize_from_email/{user}', 'Auth\LoginController@authorizeFromEmail')->name('authorizeFromEmail');
    Route::post('/liqpay_verify', 'PaymentController@verifyLiqpayPayment')->name('verifyLiqpayPayment');
});

Route::middleware('auth:api')->group(function () {
    // Загрузка дизайнгайдов
    Route::get('/download/designguide/{id}', 'DownloadController@downloadDesignguide');
    Route::get('/download/material/{id}', 'DownloadController@downloadMaterial');

    // Загрузка фалов символик
    Route::get('/download/symbolicfile/{id}', 'DownloadController@downloadSymbolicfile');
});

// Авторизация через соц сети
Route::get('/login/facebook', 'Auth\LoginController@redirectToFacebook');
Route::get('/login/facebook/callback', 'Auth\LoginController@handleFacebookCallback');

Route::get('/login/google', 'Auth\LoginController@redirectToGoogle');
Route::get('/login/google/callback', 'Auth\LoginController@handleGoogleCallback');

Route::get('/login/twitter', 'Auth\LoginController@redirectToTwitter');
Route::get('/login/twitter/callback', 'Auth\LoginController@handleTwitterCallback');

