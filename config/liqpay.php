<?php
return [
	'private_key' => env('LIQPAY_PRIVATE_KEY', ''),
	'public_key'  => env('LIQPAY_PUBLIC_KEY', ''),
	'sandbox'     => env('LIQPAY_SANDBOX', 0)
];