@servers(['web' => 'vlad@idarchive.smolensky.info'])

@task('deploy', ['on' => 'web'])
cd /var/www/idarchive
git fetch --all
git reset --hard origin/master
git pull
composer install --optimize-autoloader --no-dev
php artisan migrate --force
php artisan config:cache
npm install
npm run build
pm2 restart idarchive
@endtask